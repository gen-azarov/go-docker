#!/bin/bash

set -e

echo "Test docker build"

if [ "$BITBUCKET_BRANCH" == "master" ]; then
    docker build docker-files/prod/
fi

if [ "$BITBUCKET_BRANCH" == "develop" ]; then
    docker build docker-files/dev/
fi
