# Release notes

## 1.3.12

 * [interactive] Fix #78 in some config, interactive jobs fails

## 1.3.11

 * Update external swarm API to match latest modifications
 * [sge] Various fixes for sge executor
 * Fix permissions in pairtree storage to allow only user to access directory
 * Fixes on daemon for graceful exit
 * [kubernetes, mesos] Add project info to task metadata labels
 * [mesos] Add port mapping info in mesos, ports are in environment variables PORT_XX,
     XX being the container port (example PORT_22 for ssh)
 * [mesos] Modify task *name* information to add project name
 * Add env variables to override live_events related config
 * [ldap,goauth]
     - add possibility to use different ldap field to get user identifier
     - add possibility to define a default uidNumber/gidNumber for all users
       if LDAP has no POSIX account schema
     - #76 add fallback homeDirectory if not present in ldap (ldap/fallback_user_homeDirectory)
       defaults to null, i.e. no user home is mounted in container
 * [mesos] Fix get of container id for unified containerizer (#74)
 * Fix #75 raising an error when meta info not yet available

## 1.3.10

 * Add optional job_extra_time in config to add a sleep before ending a job to workaround some kernel/docker issues
   when job ends too quickly after some writes, leading to docker errors (defaults to none).
 * Add optional port_mapper_network_name config for cni port mapping with unified containerizer (needs cni port mapper configured on mesos slave).
 * Enhancement: add caching for user/project usage request (PR #3)
 * Add optional limits (simultaneous resource usage), see projects/limits in go-d.ini.sample. By default there is no limit
 * Fix disk usage in case of invalid symlink
 * PR #4 unify quota calculation
 * Fix active/standby management for schedulers

## 1.3.9

 * Add gpu quotas (relates to go-docker-web #33)
 * watch quotas in watcher, fix #62
 * set quotas at time based eg cpu/s, fix #61
 * Allow project owner to mount any directory if *projects/owner_mount_any_dir* is set in config (new *projects* section in go-d.ini).
 * [MESOS] Fix #65 resource offer allocation when using specific role
 * Fix go-d-clean (was not working after some previous redis related updates)
 * Trap startup error logs in job dir god_sys.err file (disk errors, setup issues, ..)
 * Allow to define default quotas in go-d.ini (see quotas section in go-d.ini.sample)


*WARNING*: quotas are now in resource/s and not in number of resource. Quota values must be set in seconds.
For example, setting 3600 to cpu resource, will allow to use 1 CPU for 3600s, or 2 CPU for 1800s, ...

## 1.3.8

* Fix Python3 bytes/str with goauth plugin

## 1.3.7

* [MESOS] If hostname from offer not in labels and unknown from initial master query, use hostname given in offer


## 1.3.6

* Fix case in watcher where task is watched multiple times and task has already been handled by a watcher
* Fix failed node information, avoid duplication of node names
* Log all failures in task information, not only last one (task.status.reasons)
* Add per user/project stats in redis db (count of total jobs)
* Add delete option in archive operations (archive API with *delete* HTTP type)
* Update Dockerfile and README to use Python 3
    Though still compatible with Python2, prefered way is to use Python3, Python2 support may not be supported in the future

## 1.3.5

* plugins/mesos.py:
  * get error message info when available from mesos
  * Register and manage DISCONNECTED and RECONNECTED events on mesoshttp lib 0.2.9
* go-d.ini: add mesos/force_pull_image optional parameter (default: True) to force to pull images before job run (even if already present)
* Fix rescheduling in case of error: was rescheduling too many times in case of success after failure
* GOD-47 FairShare scheduler: add weight support for interactive jobs
  * New go-d.ini field fairshare_interactive_weight to add a weight on ticket share for interactive jobs. If different from 0 (default=0), interactive jobs have a higher priority over non interactive jobs.
* Fix #50 redis reset issue on scheduler startup
* Fix #51 Manage mongodb AutoReconnect for HA (in case of reconnect, retry command)
* Fix #52 refactor Redis connection and add optional support for Redis Sentinel (see go-d.ini.sample, redis_sentinel section)

## 1.3.4

* goauth.py:
  * test if mail is empty in LDAP answer
  * log exception stack instead of simple error log in case of auth exception
  * code refactoring
* Define in go-d.ini fields that should be used to map user login vs ldap entries (defaults to uid or mail)
  * New ldap/ids entry in go-d.ini as an array of fields to use
* Feature #49: load user secondary groups from LDAP when available (from memberUid)
  * New optional config parameter ldap_base_dn_group_filter to specify filter in groups (defaults to ou=Groups)
* plugins/swarm.py: use latest **docker** package instead of **docker-py** (need to uninstall docker-py first)
* plugins/dockerswarm: Add Docker Swarm native mode (integrated to Docker) executor plugin
  * **WARNING**: Need to uninstall **docker-py** python package manually before installing softwate, using **docker-py** package (not compatible)
* mesos:
  * in case of task failure (task started but no success), allow restart through failure policy
  * log FAILURE and RESCIND events
  * fix API calls when gpus requirements is not set
* speedup scheduling step
  * Place only max_job_pop (go-d.ini) tasks at a time on executor

## 1.3.3

* Fix job management for job arrays

## 1.3.2

* GOD-63 Add Mesos native GPU management (adds native_gpu field under mesos section of go-d.ini)
* Fix FTP authentication
* Fix ssl usage for ldap in goauth
* Add reset argument to go-d-scheduler to reset frameworkID of mesos if necessary
* GOD-30 Remove mesos.interface and mesos.native requirements, use HTTP API of Mesos
* Add Mesos role configuration
* Add in go-d.ini optional framework name
* Add in go-d.ini optional redis_password for redis authentication

## 1.3.1

* GOD-61 Add ldaps support
  * optional change to go-d.ini ldap/ssl as boolean
* If container.id is not set, initialize it

## 1.3

* GOD-58 Add multi executor support
  * change go-d.ini: executor => executors with list of executors, add placement field for external executors additional info.
* GOD-59 Add SGE plugin to submit native or docker jobs to Sun Grid Engine
* Send job system errors (not execution failure) by watcher to stat handler

# 1.2

* GOD-49 set ldap homeDirectory field configurable via ldap/fields go-d.ini config field.
* Fix #39 mem_limit has been moved to host_config in API version 1.19
  * change go-d.ini docker_api_version => 'auto'
* Fix #40 manage tls configuration for Docker / Docker swarm
  * change go-d.ini, new *docker* section (see go-d.ini.sample)
* Manage image usage increment in watcher
* Fix #41 keep Docker image env variables when executing user script
* Kubernetes fix when job fails, reason set while not yet defined
* Fix reason failure recording
* Fix go-d-clean user disk usage update
* Fix in etcdstatus status checks
* PEP8 cleanup and fix some wrong variable use
* Move mesos_master to mesos/master config section
* Move network_disabled to network/disabled config section
* GOD-51 Support CNI networks
* GOD-35 support Mesos unified containerizer
  * New go-d.ini mesos/unified
* GOD-55 Create an archive/cleanup daemon (go-d-archive.py)
* Partial fix #43 Error if image contains a USER entry, fix for Swarm and Mesos
  * If user already exists in container, delete it
* Fix #32 manage node maintenance. If mesos node is scheduled for maintenance,
        framework will skip node and will not schedule any task on this node.
        For swarm and kubernetes, maintenance is managed directly by them.
* Fix #44 Override some go-d.ini fields with env variables (not all fields are available, see list in README.md)
* Fix #45 check config at startup (plugins_dir and shared_dir)

# 1.1

* Fix case of kill failure
* Add ldap_base_dn_filter parameter in go-d.ini to filter LDAP directory search
* GOD-34 Usage error with mesos plugin if mesos_master refers to zookeeper address
* Add dynamic reload of config via go-d-scheduler config-reload command
* Add support to access private image in Docker registry (see README.md, Private registry/images)
* Fix resource usage in Mesos
* Extract mesos failure reason for failure info
* GOD-37 Add optional requirements.uris task parameter (list of URIs), list is saved in task dir as godocker-uris.txt
* GOD-38 Add godflowwatcher watcher plugin to integrate with go-docker-flow
* Fix replay issues with port mapping
* Fix replay issues with Mesos
* Fix go-d-watcher that could lead to infinite loop preventing job killing
* GOD-44 on node failure, reschedule job (new config parameters: failure_policy, see go-d.ini.sample)
* Fix rescheduling when using private registry
* Fix Mesos frameworkID lifetime, do not expire after 7 days.
* Add optional mesos/reconcile param in go-d.ini. If enabled, at scheduler start, scheduler will check godocker running task vs mesos running task status for reconciliation.

# 1.0.4

* Add LDAP with auth
* Add debug logs
* Add install doc

# 1.0.3

* GOD-22 Add Kubernetes executor (experimental)
* GOD-23 support scheduler failover
  * high availability mode for schedulers (one active at a time)
  * allow mesos reuse of frameworkID atfer a restart or by new leader
* GOD-21 get used/available resources from executor
* GOD-20 add Consul as StatusManager
* GOD-12 Add Python3 support
* GOD-18 Support multiple sotrage managers
* GOD-26 Add Alpine Linux support
* Add optional volumes_check config param to check if a volume exists before mounting it
* Add get_quota method in auth plugins so that user quota can be extracted from an external system, auth dependant.
* GOD-28 Add possiblity to open additional ports in container
* GOD-27 Add FTP server to upload user data (read only in container), directory available in container with $GODOCKER_DATA

# 1.0.2

* minor error cases handling
* extract container id from TaskStatus in Mesos when available (mesos >= 0.23)
* add Temporary local volumes
  * Use docker-plugin-zfs Docker volume plugin
  * New configuration parameter: plugin_zfs
* add hostnames in etcd
* support optional guest users (not in system)*
* GOD-3 add node reservation support
* GOD-4 experimental resource management for GPUs
* GOD-10 allow interactive session in any image, godocker will try to install
         a ssh server if none is installed in the container.
* Fix #21 mesos start daemon failure
* GOD-14 change pairtree implementation for python 3 support
* switch ini config file to YAML format
* switch ldap library for python3 compat

# 1.0.1

* encode script to UTF-8
* add Status plugins with etcd support: add new config parameter *status_policy*
* check volumes name to catch invalid volume injection
* add global try/catch to catch errors generating an exit
* fix authorization on volumes so that config cannot be overriden by standard user
* manage case where job was not running
* store job duration from god.info file info
* manage failure to kill, switch to UNKNOWN status

# 1.0.0

 * First release
