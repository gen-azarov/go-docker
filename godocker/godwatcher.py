from godocker.daemon import Daemon
import copy
import time
import sys
import json
import logging
import logging.config
import datetime
import os
import traceback
import yaml
import urllib3

import requests

import pymongo
from pymongo import MongoClient
from bson.json_util import dumps
from influxdb import client as influxdb

from yapsy.PluginManager import PluginManager
from godocker.storageManager import StorageManager
# from godocker.iSchedulerPlugin import ISchedulerPlugin
from godocker.iExecutorPlugin import IExecutorPlugin
from godocker.iAuthPlugin import IAuthPlugin
from godocker.iWatcherPlugin import IWatcherPlugin
from godocker.iStatusPlugin import IStatusPlugin
from godocker.utils import is_array_task, is_array_child_task
import godocker.utils as godutils
from godocker.notify import Notify
from godocker import quotaTools


class GoDWatcher(Daemon):
    '''
    Can be horizontally scaled
    '''

    SIGINT = False

    def redis_call(self, f, *args, **kwargs):
        '''
        Executes a redis call, and retries connection and command in case of failure.

        Example: redis_call(self.r.hset, field, keys, 1)
        '''
        count = 0
        max_retries = 5
        while True:
            try:
                return f(*args, **kwargs)
            except Exception as e:
                self.logger.exception(str(e))
                count += 1
                # re-raise the ConnectionError if we've exceeded max_retries
                if count > max_retries:
                    raise
                self.logger.warn('Redis:Retrying in {} seconds'.format(count))
                time.sleep(count)
                self.r = self.redis_get_conn()

    def reload_config(self):
        '''
        Reload config if last reload command if recent
        '''
        config_last_update = self.redis_call(self.r.get, self.cfg['redis_prefix'] + ':config:last')
        if config_last_update is not None:
            config_last_update = float(config_last_update)
            if config_last_update > self.config_last_loaded:
                self.logger.warn('Reloading configuration')
                with open(self.config_file, 'r') as ymlfile:
                    self.cfg = yaml.load(ymlfile)
                    config_warnings = godutils.config_backward_compatibility(self.cfg)
                    if config_warnings:
                        self.logger.warn(config_warnings)
                    dt = datetime.datetime.now()
                    self.config_last_loaded = time.mktime(dt.timetuple())

    def ask_reload_config(self):
        dt = datetime.datetime.now()
        config_last_loaded = time.mktime(dt.timetuple())
        self.redis_call(self.r.set, self.cfg['redis_prefix'] + ':config:last', config_last_loaded)

    def load_config(self, f):
        '''
        Load configuration from file path
        '''
        self.config_file = f
        dt = datetime.datetime.now()
        self.config_last_loaded = time.mktime(dt.timetuple())

        self.cfg = None
        with open(f, 'r') as ymlfile:
            self.cfg = yaml.load(ymlfile)

        config_warnings = []
        try:
            config_warnings = godutils.config_backward_compatibility(self.cfg)
        except Exception as e:
            print("Invalid configuration: %s" % (str(e)))
            sys.exit(1)

        self.hostname = godutils.get_hostname()
        self.proc_name = 'watcher-' + self.hostname
        if os.getenv('GOD_PROCID'):
            self.proc_name += os.getenv('GOD_PROCID')

        self.r = self.redis_get_conn()

        self.mongo = MongoClient(self.cfg['mongo_url'])
        self.db = self.mongo[self.cfg['mongo_db']]
        self.db_jobs = self.db.jobs
        self.db_jobsover = self.db.jobsover
        self.db_users = self.db.users
        self.db_projects = self.db.projects

        self.db_influx = None
        if self.cfg['influxdb_host']:
            host = self.cfg['influxdb_host']
            port = self.cfg['influxdb_port']
            username = self.cfg['influxdb_user']
            password = self.cfg['influxdb_password']
            database = self.cfg['influxdb_db']
            self.db_influx = influxdb.InfluxDBClient(host, port, username, password, database)

        if self.cfg['log_config'] is not None:
            for handler in list(self.cfg['log_config']['handlers'].keys()):
                self.cfg['log_config']['handlers'][handler] = dict(self.cfg['log_config']['handlers'][handler])
            logging.config.dictConfig(self.cfg['log_config'])
        self.logger = logging.getLogger('godocker-watcher')
        self.logger.propagate = False

        if config_warnings:
            self.logger.warn(config_warnings)

        if not self.cfg['plugins_dir']:
            dirname, filename = os.path.split(os.path.abspath(__file__))
            self.cfg['plugins_dir'] = os.path.join(dirname, '..', 'plugins')

        self.store = StorageManager.get_storage(self.cfg)

        Notify.set_config(self.cfg)
        Notify.set_logger(self.logger)

        # Build the manager
        simplePluginManager = PluginManager()
        # Tell it the default place(s) where to find plugins
        simplePluginManager.setPluginPlaces([self.cfg['plugins_dir']])
        simplePluginManager.setCategoriesFilter({
           "Executor": IExecutorPlugin,
           "Auth": IAuthPlugin,
           "Watcher": IWatcherPlugin,
           "Status": IStatusPlugin
         })
        # Load all plugins
        simplePluginManager.collectPlugins()

        self.executors = []
        for exe in self.cfg['executors']:
            executor_name = exe
            executor = executor_name
            if self.cfg.get(executor_name, None):
                if self.cfg[executor_name].get('executor', None):
                    executor = self.cfg[executor_name]['executor']

            self.executor = None
            for pluginInfo in simplePluginManager.getPluginsOfCategory("Executor"):
                if pluginInfo.plugin_object.get_name() == executor:
                    executor = copy.deepcopy(pluginInfo.plugin_object)
                    executor.set_id(executor_name)
                    executor.set_logger(self.logger)
                    executor.set_redis_handler(self.r)
                    executor.set_jobs_handler(self.db_jobs)
                    executor.set_users_handler(self.db_users)
                    executor.set_projects_handler(self.db_projects)
                    executor.set_config(self.cfg)
                    self.executors.append(executor)
                    print("Loading executor: " + executor.get_id() + ", from " + executor.get_name())
                    break

        # Activate plugins
        self.status_manager = None
        for pluginInfo in simplePluginManager.getPluginsOfCategory("Status"):
            if 'status_policy' not in self.cfg or not self.cfg['status_policy']:
                print("No status manager in configuration")
                break
            if pluginInfo.plugin_object.get_name() == self.cfg['status_policy']:
                self.status_manager = pluginInfo.plugin_object
                self.status_manager.set_logger(self.logger)
                self.status_manager.set_redis_handler(self.r)
                self.status_manager.set_jobs_handler(self.db_jobs)
                self.status_manager.set_users_handler(self.db_users)
                self.status_manager.set_projects_handler(self.db_projects)
                self.status_manager.set_config(self.cfg)
                print("Loading status manager: " + self.status_manager.get_name())
                break

        self.watchers = []
        if 'watchers' in self.cfg and self.cfg['watchers'] is not None:
            watchers = self.cfg['watchers']
        else:
            watchers = []
        for pluginInfo in simplePluginManager.getPluginsOfCategory("Watcher"):
            if pluginInfo.plugin_object.get_name() in watchers:
                watcher = pluginInfo.plugin_object
                watcher.set_logger(self.logger)
                watcher.set_config(self.cfg)
                watcher.set_redis_handler(self.r)
                watcher.set_jobs_handler(self.db_jobs)
                watcher.set_users_handler(self.db_users)
                watcher.set_projects_handler(self.db_projects)
                self.watchers.append(watcher)
                print("Add watcher: " + watcher.get_name())

    def status(self):
        '''
        Get process status

        :return: last timestamp of keep alive for current process, else None
        '''
        if self.status_manager is None:
            return None
        status = self.status_manager.status()
        for s in status:
            if s['name'] == self.proc_name:
                return s['timestamp']
        return None

    def _set_task_exitcode(self, task, exitcode):
        '''
        Sets exit code in input task
        '''
        if task['container']['meta'] is None:
            task['container']['meta'] = {}
        if 'State' in task['container']['meta']:
            task['container']['meta']['State']['ExitCode'] = 137
        else:
            task['container']['meta'] = {
                'State': {'ExitCode': 137}
            }

    def kill_tasks(self, task_list):
        '''
        Kill tasks in list
        '''
        for task in task_list:
            if self.stop_daemon:
                for executor in self.executors:
                    executor.close()
                return
            if task is None:
                continue
            executor = godutils.get_executor(task, self.executors)
            if 'kill' not in executor.features():
                self.logger.debug('kill not supported by this executor %s for task %d' % (executor.get_id(), task['id']))
                continue
            if task['status']['primary'] == godutils.STATUS_OVER:
                continue
            if task['status']['primary'] != godutils.STATUS_PENDING:
                if is_array_task(task):
                    # If an array parent, only checks if some child tasks are still running
                    # nb_subtasks_running = self.r.get(self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':subtask')
                    # if nb_subtasks_running and int(nb_subtasks_running) > 0:
                    subtaskover = self.redis_call(self.r.get, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':subtaskover')
                    if subtaskover is None or int(subtaskover) < task['requirements']['array']['nb_tasks']:
                        over = False
                        # kill sub tasks
                        for subtask_id in task['requirements']['array']['tasks']:
                            task_to_kill = self.redis_call(self.r.get, self.cfg['redis_prefix'] + ':job:' + str(subtask_id))
                            if task_to_kill is None:
                                for i in range(5):
                                    try:
                                        task_to_kill = dumps(self.db_jobs.find_one({'id': subtask_id}))
                                        break
                                    except pymongo.errors.AutoReconnect:
                                        self.logger.warn('Mongo:AutoReconnect')
                                        time.sleep(pow(2, i))

                                self.redis_call(self.r.set, self.cfg['redis_prefix'] + ':job:' + str(subtask_id) + ':task', task_to_kill)
                                for i in range(5):
                                    try:
                                        self.db_jobs.update({'id': subtask_id},
                                                    {'$set': {
                                                            'status.secondary': godutils.STATUS_SECONDARY_KILL_REQUESTED
                                                            }
                                                })
                                        break
                                    except pymongo.errors.AutoReconnect:
                                        self.logger.warn('Mongo:AutoReconnect')
                                        time.sleep(pow(2, i))

                            self.redis_call(self.r.rpush, self.cfg['redis_prefix'] + ':jobs:kill', task_to_kill)

                    else:
                        over = True
                else:
                    (task, over) = executor.kill_task(task)

                self._set_task_exitcode(task, 137)
            else:
                if is_array_task(task):
                    # If an array parent, only checks if some child tasks are still running
                    # nb_subtasks_running = self.r.get(self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':subtask')
                    # if nb_subtasks_running and int(nb_subtasks_running) > 0:
                    subtaskover = self.redis_call(self.r.get, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':subtaskover')
                    if subtaskover is None or int(subtaskover) < task['requirements']['array']['nb_tasks']:
                        over = False
                        # kill sub tasks
                        for subtask_id in task['requirements']['array']['tasks']:
                            task_to_kill = self.redis_call(self.r.get, self.cfg['redis_prefix'] + ':job:' + str(subtask_id))
                            if task_to_kill is None:
                                for i in range(5):
                                    try:
                                        task_to_kill = dumps(self.db_jobs.find_one({'id': subtask_id}))
                                        break
                                    except pymongo.errors.AutoReconnect:
                                        self.logger.warn('Mongo:AutoReconnect')
                                        time.sleep(pow(2, i))

                                self.redis_call(self.r.set, self.cfg['redis_prefix'] + ':job:' + str(subtask_id) + ':task', task_to_kill)
                            self.redis_call(self.r.rpush, self.cfg['redis_prefix'] + ':jobs:kill', task_to_kill)
                            self.redis_call(self.r.decr, self.cfg['redis_prefix'] + ':user:' + str(task['user']['id']) + ':rate')
                    else:
                        over = True
                        self._set_task_exitcode(task, 137)
                        self.redis_call(self.r.decr, self.cfg['redis_prefix'] + ':jobs:queued')
                        self.redis_call(self.r.decr, self.cfg['redis_prefix'] + ':user:' + str(task['user']['id']) + ':rate')
                else:
                    over = True
                    self._set_task_exitcode(task, 137)
                    self.redis_call(self.r.decr, self.cfg['redis_prefix'] + ':jobs:queued')
                    self.redis_call(self.r.decr, self.cfg['redis_prefix'] + ':user:' + str(task['user']['id']) + ':rate')
            # If not over, executor could not kill the task
            if 'tentative' in task['status'] and task['status']['kill_tentative'] > 10:
                over = True
                task['status']['reason'] = 'Failed to kill task nicely, kill forced'
                self.logger.error('Kill:Force:' + str(task['id']))

            if over:
                self.logger.debug('Executor:Kill:Success:' + str(task['id']))
                original_task = None
                for i in range(5):
                    try:
                        original_task = self.db_jobs.find_one({'id': task['id']})
                        break
                    except pymongo.errors.AutoReconnect:
                        self.logger.warn('Mongo:AutoReconnect')
                        time.sleep(pow(2, i))

                if original_task is None:
                    continue
                if 'resources.port' not in executor.features():
                    executor.release_port(original_task)
                    '''
                    for port in original_task['container']['ports']:
                        host = original_task['container']['meta']['Node']['Name']
                        self.logger.debug('Port:Back:'+host+':'+str(port))
                        self.r.rpush(self.cfg['redis_prefix']+':ports:'+host, port)
                    '''

                task['container']['ports'] = []
                task['container']['port_mapping'] = []
                # If private registry was used, revert to original name without server address
                task['container']['image'] = original_task['container']['image']

                # Check for reschedule request
                if original_task and original_task['status']['secondary'] == godutils.STATUS_SECONDARY_RESCHEDULE_REQUESTED:
                    self.redis_call(self.r.delete, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':task')
                    self.redis_call(self.r.incr, self.cfg['redis_prefix'] + ':jobs:queued')
                    self.redis_call(self.r.incr, self.cfg['redis_prefix'] + ':user:' + str(task['user']['id']) + ':rate')
                    reason = ''
                    if 'reason' in task['status']:
                        reason = task['status']['reason']
                    for i in range(5):
                        try:
                            self.db_jobs.update({'id': task['id']}, {'$set': {
                                'status.primary': godutils.STATUS_PENDING,
                                'status.secondary': godutils.STATUS_SECONDARY_RESCHEDULED,
                                'status.reason': reason
                            }})
                            break
                        except pymongo.errors.AutoReconnect:
                            self.logger.warn('Mongo:AutoReconnect')
                            time.sleep(pow(2, i))
                    continue
                remove_result = None
                for i in range(5):
                    try:
                        remove_result = self.db_jobs.remove({'id': task['id']})
                        break
                    except pymongo.errors.AutoReconnect:
                        self.logger.warn('Mongo:AutoReconnect')
                        time.sleep(pow(2, i))

                if remove_result['n'] == 0:
                    # Not present anymore, may have been removed already
                    # Remove from jobs over to replace it
                    for i in range(5):
                        try:
                            self.db_jobsover.remove({'id': task['id']})
                            break
                        except pymongo.errors.AutoReconnect:
                            self.logger.warn('Mongo:AutoReconnect')
                            time.sleep(pow(2, i))

                task['status']['primary'] = godutils.STATUS_OVER
                task['status']['secondary'] = godutils.STATUS_SECONDARY_KILLED
                try:
                    if is_array_task(task):
                        status_code = self.redis_call(self.r.get, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':subtaskcode')
                        if status_code is not None:
                            task['status']['exitcode'] = int(status_code)
                        else:
                            self.logger.warn('No exit code: ' + str(task['id']))
                    else:
                        task['status']['exitcode'] = task['container']['meta']['State']['ExitCode']
                except Exception:
                    self.logger.warn('No exit code: ' + str(task['id']))
                dt = datetime.datetime.now()
                task['status']['date_over'] = time.mktime(dt.timetuple())

                task['status']['duration'] = 0
                if 'date_running' in task['status'] and task['status']['date_running'] is not None:
                    task['status']['duration'] = task['status']['date_over'] - task['status']['date_running']

                del task['_id']
                task = self.terminate_task(task)
                for i in range(5):
                    try:
                        self.db_jobsover.insert(task)
                        break
                    except pymongo.errors.AutoReconnect:
                        self.logger.warn('Mongo:AutoReconnect')
                        time.sleep(pow(2, i))

                self.redis_call(self.r.delete, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':task')
                if is_array_task(task):
                    self.redis_call(self.r.delete, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':subtaskrunning')
                    self.redis_call(self.r.delete, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':subtaskover')
                    self.redis_call(self.r.delete, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':subtask')
                    self.redis_call(self.r.delete, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':subtaskcode')
                if is_array_child_task(task):
                    subcode = 0
                    try:
                        subcode = task['container']['meta']['State']['ExitCode']
                    except Exception:
                        self.logger.debug('Could not get exit code of task')
                    self.redis_call(self.r.incr, self.cfg['redis_prefix'] + ':job:' + str(task['parent_task_id']) + ':subtaskcode', subcode)
                    self.redis_call(self.r.decr, self.cfg['redis_prefix'] + ':job:' + str(task['parent_task_id']) + ':subtaskrunning')
                    self.redis_call(self.r.decr, self.cfg['redis_prefix'] + ':job:' + str(task['parent_task_id']) + ':subtask')
                    self.redis_call(self.r.incr, self.cfg['redis_prefix'] + ':job:' + str(task['parent_task_id']) + ':subtaskover')
                    for i in range(5):
                        try:
                            self.db_jobs.update({'id': task['parent_task_id']}, {'$inc': {'requirements.array.nb_tasks_over': 1}})
                            break
                        except pymongo.errors.AutoReconnect:
                            self.logger.warn('Mongo:AutoReconnect')
                            time.sleep(pow(2, i))

                if not is_array_task(task):
                    self.update_user_usage(task)

                if not is_array_child_task(task):
                    self.notify_msg(task)
                    Notify.notify_email(task)
            else:
                if 'kill_tentative' not in task['status']:
                    task['status']['kill_tentative'] = 0
                task['status']['kill_tentative'] += 1
                # Could not kill, put back in queue
                if over is not None:
                    # async case like mesos, kill is not immediate
                    self.logger.warn('Executor:Kill:Error:' + str(task['id']))
                if task['status']['kill_tentative'] > 10:
                    # Failed to kill after 10 tentatives
                    # Remove kill status
                    self.logger.error('Executor:Kill:Error:' + str(task['id']))
                    for i in range(5):
                        try:
                            self.db_jobs.update({'id': task['id']},
                                                {'$set': {
                                                    'status.secondary': godutils.STATUS_SECONDARY_UNKNOWN,
                                                    'status.reason': 'Failure to kill job'

                                                }})
                            break
                        except pymongo.errors.AutoReconnect:
                            self.logger.warn('Mongo:AutoReconnect')
                            time.sleep(pow(2, i))

                else:
                    self.redis_call(self.r.rpush, self.cfg['redis_prefix'] + ':jobs:kill', dumps(task))

    def suspend_tasks(self, suspend_list):
        '''
        Suspend/pause tasks in list
        '''
        if self.stop_daemon:
            for executor in self.executors:
                executor.close()
            return

        for task in suspend_list:
            if self.stop_daemon:
                for executor in self.executors:
                    executor.close()
                return

            status = None
            over = False

            executor = godutils.get_executor(task, self.executors)

            if 'pause' not in executor.features():
                self.logger.debug('suspend not supported by this executor %s for task %d' % (executor.get_id(), task['id']))
                continue

            if task['status']['primary'] == godutils.STATUS_PENDING or task['status']['primary'] == godutils.STATUS_OVER:
                status = godutils.STATUS_SECONDARY_SUSPEND_REJECTED
                over = True
            elif is_array_task(task):
                # suspend not supported for array_tasks
                status = godutils.STATUS_SECONDARY_SUSPEND_REJECTED
                over = True
            else:
                (task, over) = executor.suspend_task(task)
                status = godutils.STATUS_SECONDARY_SUSPENDED
                Notify.notify_email(task)
            if over:
                task['status']['secondary'] = status
                self.redis_call(self.r.set, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':task', dumps(task))
                if task['status']['primary'] != godutils.STATUS_OVER:
                    for i in range(5):
                        try:
                            self.db_jobs.update({'id': task['id']}, {'$set': {'status.secondary': status}})
                            break
                        except pymongo.errors.AutoReconnect:
                            self.logger.warn('Mongo:AutoReconnect')
                            time.sleep(pow(2, i))

            else:
                # Could not kill, put back in queue
                self.logger.warn('Executor:Suspend:Error:' + str(task['id']))
                self.redis_call(self.r.rpush, self.cfg['redis_prefix'] + ':jobs:suspend', dumps(task))

    def resume_tasks(self, resume_list):
        '''
        Resume tasks in list
        '''
        if self.stop_daemon:
            for executor in self.executors:
                executor.close()
            return
        for task in resume_list:
            if self.stop_daemon:
                for executor in self.executors:
                    executor.close()
                return
            status = None
            over = False

            executor = godutils.get_executor(task, self.executors)
            if 'pause' not in executor.features():
                self.logger.debug('resume not supported by this executor %s for task %d' % (executor.get_id(), task['id']))
                continue

            if task['status']['primary'] == godutils.STATUS_PENDING or task['status']['primary'] == godutils.STATUS_OVER or task['status']['secondary'] != 'resume requested':
                status = godutils.STATUS_SECONDARY_RESUME_REJECTED
                over = True
            else:
                (task, over) = executor.resume_task(task)
                status = godutils.STATUS_SECONDARY_RESUMED
                Notify.notify_email(task)
            if over:
                task['status']['secondary'] = status
                self.redis_call(self.r.set, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':task', dumps(task))
                if task['status']['primary'] != godutils.STATUS_OVER:
                    for i in range(5):
                        try:
                            self.db_jobs.update({'id': task['id']}, {'$set': {'status.secondary': status}})
                            break
                        except pymongo.errors.AutoReconnect:
                            self.logger.warn('Mongo:AutoReconnect')
                            time.sleep(pow(2, i))
            else:
                # Could not resumed, put back in queue
                self.logger.warn('Executor:Resume:Error:' + str(task['id']))
                self.redis_call(self.r.rpush, self.cfg['redis_prefix'] + ':jobs:resume', dumps(task))

    def _add_to_stats(self, task):
        '''
        Add task to stats db
        '''
        if self.db_influx is None:
            return
        task_duration = 0
        if task['status']['date_running'] and task['status']['date_over']:
            task_duration = task['status']['date_over'] - task['status']['date_running']
        task_waiting = 0
        if task['status']['date_running']:
            task_waiting = task['status']['date_running'] - task['date']

        data = [{
            'points': [[
                task['user']['id'],
                task['requirements']['cpu'],
                task['requirements']['ram'],
                task_duration,
                task_waiting,
                task['container']['image']
            ]],
            'name':'god_task_usage',
            'columns': ["user", "cpu", "ram", "durationtime", "waitingtime", "image"]
        }]
        try:
            self.db_influx.write_points(data)
        except Exception as e:
            # Do not fail on stat writing
            self.logger.error('Stat:Error:' + str(e))

    def terminate_task(self, task):
        '''
        Checks and updates on finished task (ok or killed)

        :param task: current task
        :type task: Task
        :return: updated task
        '''
        if 'disk_default_quota' in self.cfg and self.cfg['disk_default_quota'] is not None:
            folder_size = 0
            if not is_array_task(task):
                task_dir = self.store.get_task_dir(task)
                folder_size = godutils.get_folder_size(task_dir)
            if 'meta' not in task['container'] or task['container']['meta'] is None:
                task['container']['meta'] = {}
            task['container']['meta']['disk_size'] = folder_size
            for i in range(5):
                try:
                    self.db_users.update({'id': task['user']['id']}, {'$inc': {'usage.disk': folder_size}})
                    break
                except pymongo.errors.AutoReconnect:
                    self.logger.warn('Mongo:AutoReconnect')
                    time.sleep(pow(2, i))

        if 'guest' in task['user'] and task['user']['guest']:
            # Calculate pseudo home dir size
            for volume in task['container']['volumes']:
                if volume['name'] == 'home':
                    print("Calculate for " + volume['path'])
                    folder_size = godutils.get_folder_size(volume['path'])
                    for i in range(5):
                        try:
                            self.db_users.update({'id': task['user']['id']}, {'$set': {'usage.guest_home': folder_size}})
                            break
                        except pymongo.errors.AutoReconnect:
                            self.logger.warn('Mongo:AutoReconnect')
                            time.sleep(pow(2, i))
                    break
        # Increment image usage
        if 'reason' not in task['status'] or task['status']['reason'] is None:
            self.redis_call(self.r.hincrby, self.cfg['redis_prefix'] + ':images', task['container']['image'], 1)
        # Incr project and user stats
        self.redis_call(self.r.incr, self.cfg['redis_prefix'] + ':jobs:stats:user:' + str(task['user']['id']))
        self.redis_call(self.r.incr, self.cfg['redis_prefix'] + ':jobs:stats:project:' + str(task['user']['project']))
        for watcher in self.watchers:
            watcher.done(task)

        if task.get('hooks', None):
            try:
                if task['hooks'].get('ok', None) and task['status']['exitcode'] == 0:
                    requests.post(task['hooks']['ok'], json=task['status'])
                elif task['hooks'].get('ko', None):
                    requests.post(task['hooks']['ko'], json=task['status'])
            except Exception as e:
                logging.exception('Failed to contact hook ' + str(task['hooks']) + ': ' + str(e))
        return task

    def update_user_usage(self, task):
        '''
        Add to user usage task consumption (cpu,ram,time)
        '''
        task_duration = 0
        if 'date_running' in task['status'] and task['status']['date_running'] and task['status']['date_over']:
            task_duration = task['status']['date_over'] - task['status']['date_running']

        if 'gpus' not in task['requirements']:
            task['requirements']['gpus'] = 0

        # Set an RDD like over a time window of self.cfg['user_reset_usage_duration'] days
        dt = datetime.datetime.now()
        date_key = str(dt.year) + '_' + str(dt.month) + '_' + str(dt.day)
        set_expire = True
        if self.redis_call(self.r.exists, self.cfg['redis_prefix'] + ':user:' + str(task['user']['id']) + ':cpu:' + date_key):
            set_expire = False
        self.redis_call(self.r.incrbyfloat, self.cfg['redis_prefix'] + ':user:' + str(task['user']['id']) + ':cpu:' + date_key, task['requirements']['cpu'] * task_duration)
        self.redis_call(self.r.incrbyfloat, self.cfg['redis_prefix'] + ':user:' + str(task['user']['id']) + ':ram:' + date_key, task['requirements']['ram'] * task_duration)
        self.redis_call(self.r.incrbyfloat, self.cfg['redis_prefix'] + ':user:' + str(task['user']['id']) + ':gpu:' + date_key, task['requirements']['gpus'] * task_duration)
        self.redis_call(self.r.incrbyfloat, self.cfg['redis_prefix'] + ':user:' + str(task['user']['id']) + ':time:' + date_key, task_duration)
        if set_expire:
            expiration_time = self.cfg['user_reset_usage_duration'] * 24 * 3600
            self.redis_call(self.r.expire, self.cfg['redis_prefix'] + ':user:' + str(task['user']['id']) + ':cpu:' + date_key, expiration_time)
            self.redis_call(self.r.expire, self.cfg['redis_prefix'] + ':user:' + str(task['user']['id']) + ':ram:' + date_key, expiration_time)
            self.redis_call(self.r.expire, self.cfg['redis_prefix'] + ':user:' + str(task['user']['id']) + ':time:' + date_key, expiration_time)
            self.redis_call(self.r.expire, self.cfg['redis_prefix'] + ':user:' + str(task['user']['id']) + ':gpu:' + date_key, expiration_time)

        set_expire = True
        if self.redis_call(self.r.exists, self.cfg['redis_prefix'] + ':group:' + str(task['user']['project']) + ':cpu:' + date_key):
            set_expire = False
        self.redis_call(self.r.incrbyfloat, self.cfg['redis_prefix'] + ':group:' + str(task['user']['project']) + ':cpu:' + date_key, task['requirements']['cpu'] * task_duration)
        self.redis_call(self.r.incrbyfloat, self.cfg['redis_prefix'] + ':group:' + str(task['user']['project']) + ':ram:' + date_key, task['requirements']['ram'] * task_duration)
        self.redis_call(self.r.incrbyfloat, self.cfg['redis_prefix'] + ':group:' + str(task['user']['project']) + ':gpu:' + date_key, task['requirements']['gpus'] * task_duration)
        self.redis_call(self.r.incrbyfloat, self.cfg['redis_prefix'] + ':group:' + str(task['user']['project']) + ':time:' + date_key, task_duration)
        if set_expire:
            expiration_time = self.cfg['user_reset_usage_duration'] * 24 * 3600
            self.redis_call(self.r.expire, self.cfg['redis_prefix'] + ':group:' + str(task['user']['project']) + ':cpu:' + date_key, expiration_time)
            self.redis_call(self.r.expire, self.cfg['redis_prefix'] + ':group:' + str(task['user']['project']) + ':gpu:' + date_key, expiration_time)
            self.redis_call(self.r.expire, self.cfg['redis_prefix'] + ':group:' + str(task['user']['project']) + ':ram:' + date_key, expiration_time)
            self.redis_call(self.r.expire, self.cfg['redis_prefix'] + ':group:' + str(task['user']['project']) + ':time:' + date_key, expiration_time)

    def notify_msg(self, task):
        if not self.cfg['live_events']:
            return
        status = task['status']['primary']
        if task['status']['secondary'] == godutils.STATUS_SECONDARY_KILLED:
            status = godutils.STATUS_SECONDARY_KILLED
        self.redis_call(self.r.publish, self.cfg['redis_prefix'] + ':jobs:pubsub', dumps({
            'user': task['user']['id'],
            'id': task['id'],
            'status': status,
            'name': task['meta']['name']
        }))

    def _prometheus_error_stats(self, host='none', executor='none'):
        if 'prometheus_key' in self.cfg and self.cfg['prometheus_exporter'] is not None and self.cfg['prometheus_key'] is not None:
            try:
                http = urllib3.PoolManager()
                r = http.request('POST',
                    self.cfg['prometheus_exporter'] + '/api/1.0/prometheus',
                    headers={'Content-Type': 'application/json'},
                    body=json.dumps({'key': self.cfg['prometheus_key'],
                                      'stat': [{'name': 'god_watcher_job_error', 'value': 1, 'host': host, 'executor': executor}]}))
                if r.status != 200:
                    logging.warn('Prometheus:Failed to send stats')
            except Exception as e:
                logging.warn("Prometheus:Failed to send stats: " + str(e))

    def _prometheus_duration_stats(self, duration, host='none', executor='none'):
        if 'prometheus_key' in self.cfg and self.cfg['prometheus_exporter'] is not None and self.cfg['prometheus_key'] is not None:
            try:
                http = urllib3.PoolManager()
                r = http.request('POST',
                    self.cfg['prometheus_exporter'] + '/api/1.0/prometheus',
                    headers={'Content-Type': 'application/json'},
                    body=json.dumps({'key': self.cfg['prometheus_key'],
                                      'stat': [{'name': 'god_watcher_job_duration', 'value': duration, 'host': host, 'executor': executor}]}))
                if r.status != 200:
                    logging.warn('Prometheus:Failed to send stats')
            except Exception as e:
                logging.warn("Prometheus:Failed to send stats: " + str(e))

    def check_running_jobs(self):
        '''
        Checks if running jobs are over
        '''
        self.logger.debug("Check running jobs")

        nb_running_jobs = self.redis_call(self.r.llen, self.cfg['redis_prefix'] + ':jobs:running')

        nb_elt = 0
        while True and not self.stop_daemon:
            try:
                task_id = None
                elt = None
                if nb_elt < self.cfg['max_job_pop'] and nb_elt < nb_running_jobs:
                    task_id = self.redis_call(self.r.lpop, self.cfg['redis_prefix'] + ':jobs:running')
                    if not task_id:
                        return
                    elt = self.redis_call(self.r.get, self.cfg['redis_prefix'] + ':job:' + str(task_id) + ':task')
                    nb_elt += 1
                else:
                    break

                if not elt:
                    return
                task = json.loads(elt)
                executor = godutils.get_executor(task, self.executors)
                # Has been killed in the meanwhile, already managed
                if not self.redis_call(self.r.get, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':task'):
                    continue

                original_task = None
                for i in range(5):
                    try:
                        original_task = self.db_jobs.find_one({'id': task['id']})
                        break
                    except pymongo.errors.AutoReconnect:
                        self.logger.warn('Mongo:AutoReconnect')
                        time.sleep(pow(2, i))
                if original_task is None:
                    logging.info("Task " + str(task['id']) + " already managed and over, skipping...")
                    # delete from redis, just in case....
                    self.redis_call(self.r.delete, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':task')
                    continue

                # If some dynamic fields are present, check for changes
                if 'dynamic_fields' in self.cfg and self.cfg['dynamic_fields']:
                    mongo_task = None
                    for i in range(5):
                        try:
                            mongo_task = self.db_jobs.find_one({'id': task['id']})
                            break
                        except pymongo.errors.AutoReconnect:
                            self.logger.warn('Mongo:AutoReconnect')
                            time.sleep(pow(2, i))

                    if mongo_task:
                        for dynamic_field in self.cfg['dynamic_fields']:
                            if dynamic_field['name'] in mongo_task['requirements']:
                                task['requirements'][dynamic_field['name']] = mongo_task['requirements'][dynamic_field['name']]

                if is_array_task(task):
                    # If an array parent, only checks if some child tasks are still running
                    # nb_subtasks_running = self.r.get(self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':subtaskrunning')
                    subtaskover = self.redis_call(self.r.get, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':subtaskover')
                    if subtaskover and int(subtaskover) == task['requirements']['array']['nb_tasks']:
                        over = True
                    else:
                        over = False
                    '''
                    if nb_subtasks_running is None or int(nb_subtasks_running) > 0:
                        over = False
                    else:
                        over = True
                    '''
                else:
                    (task, over) = executor.watch_tasks(task)
                    if task is None:
                        self.logger.debug('TASK:' + str(task_id) + ':Cleaned by executor')
                        continue
                self.logger.debug("TASK:" + str(task['id']) + ":" + str(over))
                if over:
                    # If private registry was used, revert to original name without server address
                    task['container']['image'] = original_task['container']['image']
                    # Id, with mesos, is set after task submission, so not present in runtime task
                    # We need to get it from database metadata
                    if 'id' in original_task['container'] and original_task['container']['id']:
                        task['container']['id'] = original_task['container']['id']

                    # Free ports
                    # Put back mapping allocated ports if not managed by executor
                    if 'resources.port' not in executor.features():
                        executor.release_port(original_task)
                        '''
                        for port in original_task['container']['ports']:
                            host = original_task['container']['meta']['Node']['Name']
                            self.logger.debug('Port:Back:'+host+':'+str(port))
                            self.r.rpush(self.cfg['redis_prefix']+':ports:'+host, port)
                        '''
                    task['container']['ports'] = []

                    if 'failure_policy' not in self.cfg:
                        self.cfg['failure_policy'] = {'strategy': 0, 'skip_failed_nodes': False}

                    failed_node = None
                    if 'failure' in task['status'] and task['status']['failure']['reason'] is not None:
                        if 'Node' in task['container']['meta'] and 'Name' in task['container']['meta']['Node']:
                            failed_node = task['container']['meta']['Node']['Name']
                            # Send stat
                            self._prometheus_error_stats(failed_node, executor=executor.get_id())

                    if 'failure_policy' not in task['requirements'] or task['requirements']['failure_policy'] > self.cfg['failure_policy']['strategy']:
                        task['requirements']['failure_policy'] = self.cfg['failure_policy']['strategy']
                    # Should it be rescheduled ni case of node crash/error?
                    if task['requirements']['failure_policy'] > 0 and original_task['status']['secondary'] != godutils.STATUS_SECONDARY_KILL_REQUESTED:
                        # We have a reason failure and not reached max number of restart
                        if 'failure' in task['status'] and task['status']['failure']['reason'] is not None:
                            if 'failure' not in original_task['status']:
                                original_task['status']['failure'] = {'nodes': [], 'reasons': [], 'reason': None, 'count': 0}

                            task['status']['failure']['reasons'] = original_task['status']['failure']['reasons']
                            dt = datetime.datetime.now()
                            ts = time.mktime(dt.timetuple())
                            task['status']['failure']['reasons'].append({'ts': ts, 'msg': task['status']['failure']['reason'], 'node': failed_node})

                            task['status']['failure']['count'] = original_task['status']['failure']['count'] + 1
                            if failed_node:
                                task['status']['failure']['nodes'] = original_task['status']['failure']['nodes']
                                task['status']['failure']['nodes'].append(failed_node)

                            if original_task['status']['failure']['count'] < task['requirements']['failure_policy']:

                                self.logger.debug('Error:Reschedule:' + str(task['id']) + ":" + str(task['status']['failure']['count']))
                                self.redis_call(self.r.delete, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':task')
                                self.redis_call(self.r.incr, self.cfg['redis_prefix'] + ':jobs:queued')
                                self.redis_call(self.r.incr, self.cfg['redis_prefix'] + ':user:' + str(task['user']['id']) + ':rate')
                                reason = ''
                                if 'reason' in task['status']:
                                    reason = task['status']['reason']
                                for i in range(5):
                                    try:
                                        self.db_jobs.update({'id': task['id']}, {'$set': {
                                            'status.primary': godutils.STATUS_PENDING,
                                            'status.secondary': godutils.STATUS_SECONDARY_RESCHEDULED,
                                            'status.reason': reason,
                                            'status.failure': task['status']['failure']
                                        }})
                                        break
                                    except pymongo.errors.AutoReconnect:
                                        self.logger.warn('Mongo:AutoReconnect')
                                        time.sleep(pow(2, i))

                                self.redis_call(self.r.delete, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':task')
                                continue

                    if is_array_task(task):
                        self.redis_call(self.r.delete, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':subtaskrunning')
                        self.redis_call(self.r.delete, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':subtaskover')
                        self.redis_call(self.r.delete, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':subtaskcode')
                        task['requirements']['array']['nb_tasks_over'] = task['requirements']['array']['nb_tasks']
                    if is_array_child_task(task):
                        subcode = 0
                        try:
                            subcode = task['container']['meta']['State']['ExitCode']
                        except Exception:
                            self.logger.debug('Could not get exit code of task')
                        self.redis_call(self.r.incr, self.cfg['redis_prefix'] + ':job:' + str(task['parent_task_id']) + ':subtaskover')
                        self.redis_call(self.r.incr, self.cfg['redis_prefix'] + ':job:' + str(task['parent_task_id']) + ':subtaskcode', subcode)
                        self.redis_call(self.r.decr, self.cfg['redis_prefix'] + ':job:' + str(task['parent_task_id']) + ':subtaskrunning')
                        for i in range(5):
                            try:
                                self.db_jobs.update({'id': task['parent_task_id']}, {'$inc': {'requirements.array.nb_tasks_over': 1}})
                                break
                            except pymongo.errors.AutoReconnect:
                                self.logger.warn('Mongo:AutoReconnect')
                                time.sleep(pow(2, i))

                    remove_result = None
                    for i in range(5):
                        try:
                            remove_result = self.db_jobs.remove({'id': task['id']})
                            break
                        except pymongo.errors.AutoReconnect:
                            self.logger.warn('Mongo:AutoReconnect')
                            time.sleep(pow(2, i))

                    if remove_result is not None and remove_result['n'] == 0:
                        # Not present anymore, may have been removed already
                        # Remove from jobs over to replace it
                        for i in range(5):
                            try:
                                self.db_jobsover.remove({'id': task['id']})
                                break
                            except pymongo.errors.AutoReconnect:
                                self.logger.warn('Mongo:AutoReconnect')
                                time.sleep(pow(2, i))

                    task['status']['primary'] = godutils.STATUS_OVER
                    task['status']['secondary'] = ''

                    if 'failure' in original_task['status']:
                        task['status']['failure'] = original_task['status']['failure']

                    try:
                        task['status']['exitcode'] = task['container']['meta']['State']['ExitCode']
                    except Exception:
                        self.logger.warn('No exit code: ' + str(task['id']))
                    dt = datetime.datetime.now()
                    task['status']['date_over'] = time.mktime(dt.timetuple())

                    task['status']['duration'] = task['status']['date_over'] - task['status']['date_running']
                    task_dir = self.store.get_task_dir(task)
                    god_info_file = os.path.join(task_dir, 'god.info')
                    if os.path.exists(god_info_file):
                        with open(god_info_file) as f:
                            content = f.read().splitlines()
                        try:
                            task['status']['duration'] = int(content[1]) - int(content[0])
                        except Exception:
                            self.logger.warn('Failed to read god.info data: ' + god_info_file)

                    del task['_id']
                    task = self.terminate_task(task)
                    for i in range(5):
                        try:
                            self.db_jobsover.insert(task)
                            break
                        except pymongo.errors.AutoReconnect:
                            self.logger.warn('Mongo:AutoReconnect')
                            time.sleep(pow(2, i))

                    self.redis_call(self.r.delete, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':task')
                    if not is_array_task(task):
                        self.update_user_usage(task)
                        self._add_to_stats(task)
                    if not is_array_child_task(task):
                        self.notify_msg(task)
                        Notify.notify_email(task)

                    try:
                        if task['container']['meta'] is not None and 'Node' in task['container']['meta'] and 'Name' in task['container']['meta']['Node']:
                            task_host = task['container']['meta']['Node']['Name']
                            # Send stat
                            self._prometheus_duration_stats(task['status']['duration'], host=task_host, executor=executor.get_id())
                    except Exception:
                        self.logger.exception('failed to send duration stats')
                else:
                    can_run = True
                    # Check for quotas
                    quota_exceeded = self.__is_quota_exceeded(task)
                    if quota_exceeded:
                        can_run = False
                    if can_run:
                        for watcher in self.watchers:
                            if watcher.can_run(task) is None:
                                can_run = False
                                break
                    if can_run:
                        self.redis_call(self.r.set, self.cfg['redis_prefix'] + ':job:' + str(task['id']) + ':task', dumps(task))
                        self.redis_call(self.r.rpush, self.cfg['redis_prefix'] + ':jobs:running', task['id'])
            except KeyboardInterrupt:
                self.logger.warn('Interrupt received, exiting after cleanup')
                self.redis_call(self.r.rpush, self.cfg['redis_prefix'] + ':jobs:running', task['id'])
                sys.exit(0)

    def __is_quota_exceeded(self, task):
        '''
        check if task quota is exceeded

        :param task: task to check
        :type task: Task
        :return: true is quota reached and kill the task, else return false
        '''
        executor = godutils.get_executor(task, self.executors)

        self.logger.debug('Check for quotas ' + str(task['id']))
        if 'kill_on_limit' not in self.cfg['quotas'] or not self.cfg['quotas']['kill_on_limit']:
            self.logger.debug('kill on limit not active, skipping checks')
            return False

        dt = datetime.datetime.now()
        now = time.mktime(dt.timetuple())
        duration = 0
        if 'date_running' in task['status'] and task['status']['date_running'] is not None:
                duration = now - task['status']['date_running']
        if not duration:
            self.logger.debug('no duration yet, skipping quota control')
            return False

        reject = quotaTools.is_quota_exceeded(task, executor.get_user_usage, duration)

        if reject:
            self.logger.info('Quotas exceeded, killing ' + str(task['id']))
            task['status']['reason'] = 'Quota reached'
            self.kill_tasks([task])
        else:
            self.logger.debug('Quotas ok, task can run')
        return reject

    def manage_tasks(self):
        '''
        Schedule and run tasks / kill tasks

        '''
        self.logger.debug('Watcher:' + str(self.hostname) + ':Run')
        self.logger.debug("Get tasks to kill")
        if self.stop_daemon:
            for executor in self.executors:
                executor.close()
            return
        kill_task_list = []
        kill_task_length = self.redis_call(self.r.llen, self.cfg['redis_prefix'] + ':jobs:kill')
        for i in range(min(kill_task_length, self.cfg['max_job_pop'])):
            task = self.redis_call(self.r.lpop, self.cfg['redis_prefix'] + ':jobs:kill')
            if task and task != 'None':
                kill_task_list.append(json.loads(task))

        self.kill_tasks(kill_task_list)

        self.logger.debug('Get tasks to suspend')
        if self.stop_daemon:
            for executor in self.executors:
                executor.close()
            return
        suspend_task_list = []
        suspend_task_length = self.redis_call(self.r.llen, self.cfg['redis_prefix'] + ':jobs:suspend')
        for i in range(min(suspend_task_length, self.cfg['max_job_pop'])):
            task = self.redis_call(self.r.lpop, self.cfg['redis_prefix'] + ':jobs:suspend')
            if task:
                suspend_task_list.append(json.loads(task))

        self.suspend_tasks(suspend_task_list)

        self.logger.debug('Get tasks to resume')
        if self.stop_daemon:
            for executor in self.executors:
                executor.close()
            return
        resume_task_list = []
        resume_task_length = self.redis_call(self.r.llen, self.cfg['redis_prefix'] + ':jobs:resume')
        for i in range(min(resume_task_length, self.cfg['max_job_pop'])):
            task = self.redis_call(self.r.lpop, self.cfg['redis_prefix'] + ':jobs:resume')
            if task:
                resume_task_list.append(json.loads(task))

        self.resume_tasks(resume_task_list)

        self.logger.debug('Look for terminated jobs')
        if self.stop_daemon:
            for executor in self.executors:
                executor.close()
            return
        self.check_running_jobs()

    def signal_handler(self, signum, frame):
        super(GoDWatcher, self).signal_handler(signum, frame)
        GoDWatcher.SIGINT = True
        self.logger.warn('User request to exit')
        for executor in self.executors:
            executor.close()

    def update_status(self):
        if self.status_manager is None:
            return
        if self.status_manager is not None:
            res = self.status_manager.keep_alive(self.proc_name, 'watcher')
            if not res:
                self.logger.error('Watcher:UpdateStatus:Error')
        return

    def run(self, loop=True):
        '''
        Main executor loop

        '''
        self.hostname = None
        infinite = True
        for executor in self.executors:
            executor.open(1)
        self.logger.warn('Start watcher')
        while infinite and True and not GoDWatcher.SIGINT:
                # Schedule timer
            try:
                self.update_status()
            except Exception as e:
                self.logger.error('Watcher:' + str(self.hostname) + ':' + str(e))
                traceback_msg = traceback.format_exc()
                self.logger.error(traceback_msg)
            try:
                self.manage_tasks()
            except Exception as e:
                self.logger.error('Watcher:' + str(self.hostname) + ':' + str(e))
                traceback_msg = traceback.format_exc()
                self.logger.error(traceback_msg)
            self.reload_config()
            time.sleep(2)
            if not loop:
                infinite = False
        for executor in self.executors:
            executor.close()
