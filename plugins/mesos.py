from godocker.iExecutorPlugin import IExecutorPlugin
import json
import time
import os
import threading
import urllib3
import base64
import pymongo
from sys import version_info

from bson.json_util import dumps

from mesoshttp.client import MesosClient


class MesosFramework(threading.Thread):
    '''
    Thread managing Mesos framework communication
    '''

    def __init__(self, client):
        threading.Thread.__init__(self)
        self.client = client
        self.stop = False

    def run(self):
        try:
            self.client.register()
        except KeyboardInterrupt:
            print('Stop requested by user, stopping framework....')


class Mesos(IExecutorPlugin):
    '''
    Mesos executor to manage task submission and status
    '''

    def get_name(self):
        return "mesos"

    def get_type(self):
        return "Executor"

    def features(self):
        '''
        Get supported features

        :return: list of features within ['kill', 'pause','resources.port', 'cni']
        '''
        return ['kill', 'resources.port', 'cni', 'interactive', 'gpus']

    def set_config(self, cfg):
        self.cfg = cfg
        self.Terminated = False
        self.driver = None
        self.mesos_connected = False
        self.prefix = 'mesos'
        self.prefix += ':' + self.get_id()
        if self.redis_handler is None:
            self.redis_handler = self.redis_get_conn()
            '''
            r_pwd = None
            if 'redis_password' in self.cfg:
                r_pwd = self.cfg['redis_password']
            self.redis_handler = redis.StrictRedis(
                host=self.cfg['redis_host'],
                port=self.cfg['redis_port'],
                db=self.cfg['redis_db'],
                password=r_pwd,
                decode_responses=True
            )
            '''

    def has_enough_resource(self, offer, requested_resource, quantity):
        '''
        Checks if a resource is available and has enough slots available in offer

        :param offer: Mesos offer
        :type offer: Mesos Offer
        :param requested_resource: requested resource name as defined in mesos-slave resource (resource==gpu)
        :type requested_resource: str
        :param quantity: number of slots requested_resource
        :type quantity: int
        :return: True if resource is available in offer, else False
        '''
        available_resources = 0
        for resource in offer['resources']:
            if resource.name == requested_resource:
                for mesos_range in resource.ranges.range:
                    if mesos_range.begin <= mesos_range.end:
                        available_resources += 1 + mesos_range.end - mesos_range.begin
        if available_resources >= quantity:
            return True
        else:
            return False

    def get_mapping_port(self, offer, task):
        '''
        Get a port mapping for interactive tasks

        :param task: task
        :type task: int
        :return: available port
        '''

        # Get first free port
        port = None
        for resource in offer['resources']:
            if resource['name'] == "ports":
                for mesos_range in resource['ranges']['range']:
                    if mesos_range['begin'] <= mesos_range['end']:
                        port = str(mesos_range['begin'])
                        mesos_range['begin'] += 1
                        break
        if port is None:
            return None
        self.logger.debug(
            'Port:Give:' +
            task['container']['meta']['Node']['Name'] +
            ':' +
            str(port)
        )
        if 'ports' not in task['container']:
            task['container']['ports'] = []
        task['container']['ports'].append(port)
        return int(port)

    def plugin_zfs_unmount(self, hostname, task):
        '''
        Unmount and delete temporary storage
        '''
        if 'plugin_zfs' not in self.cfg or not self.cfg['plugin_zfs']:
            return

        if 'tmpstorage' not in task['requirements']:
            return

        if task['requirements']['tmpstorage'] is None or task['requirements']['tmpstorage']['size'] == '':
            return

        try:
            http = urllib3.PoolManager()
            r = http.urlopen(
                'POST',
                'http://' + hostname + ':5000/VolumeDriver.Unmount',
                body=json.dumps({'Name': str(task['id'])})
            )
            res = json.loads(r.data)
            if res['Err'] is not None or res['Err'] != '':
                r = http.urlopen(
                    'POST',
                    'http://' + hostname + ':5000/VolumeDriver.Remove',
                    body=json.dumps({'Name': str(task['id'])})
                )
            else:
                self.logger.error(
                    'Failed to remove zfs volume: ' + str(task['id'])
                )
        except Exception:
            self.logger.error('Failed to remove zfs volume: ' + str(task['id']))

    def plugin_zfs_mount(self, hostname, task):
        '''
        Create and mount temporary storage
        '''
        zfs_path = None

        if 'plugin_zfs' not in self.cfg or not self.cfg['plugin_zfs']:
            return (True, None)

        if 'tmpstorage' not in task['requirements']:
            return (True, None)

        if task['requirements']['tmpstorage'] is None or task['requirements']['tmpstorage']['size'] == '':
            return (True, None)

        try:
            http = urllib3.PoolManager()
            r = None
            activated = self.redis_call(self.redis_handler.get, self.cfg['redis_prefix'] + ':plugins:zfs:' + hostname)
            if activated is None:
                http.urlopen(
                    'GET',
                    'http://' + hostname + ':5000/Plugin.Activate'
                )
                self.redis_call(self.redis_handler.set,
                    self.cfg['redis_prefix'] + ':plugins:zfs:' + hostname,
                    "plugin-zfs"
                )
            r = http.urlopen(
                'POST',
                'http://' + hostname + ':5000/VolumeDriver.Create',
                body=json.dumps({
                    'Name': str(task['id']),
                    'Opts': {
                        'size': str(task['requirements']['tmpstorage']['size'])
                    }
                })
            )
            if r.status == 200:
                r = http.urlopen(
                    'POST',
                    'http://' + hostname + ':5000/VolumeDriver.Mount',
                    body=json.dumps({'Name': str(task['id'])})
                )
                res = json.loads(r.data)
                if res['Err'] is not None:
                    return (False, None)
                r = http.urlopen(
                    'POST',
                    'http://' + hostname + ':5000/VolumeDriver.Path',
                    body=json.dumps({'Name': str(task['id'])})
                )
                res = json.loads(r.data)
                zfs_path = res['Name']
            else:
                self.logger.error("Failed to resource plugin-zfs")
                return (False, None)
        except Exception as e:
            self.logger.error("Failed to resource plugin-zfs:" + str(e))
            return (False, None)
        return (True, zfs_path)

    def subscribed(self, driver):
        self.logger.debug('SUBSCRIBED: ' + str(driver))
        self.mesos_connected = True
        self.driver = driver
        self.frameworkId = driver.frameworkId
        self.logger.info("Registered with framework ID %s" % self.frameworkId)
        self.logger.debug('StreamId: %s' % (self.driver.streamId))
        master_ip = None
        master_url = self.client.mesos_url.replace('http://', '').split(':')
        master_address = master_url[0]
        master_port = master_url[1]

        try:
            master_address = self.client.master_info['address']['hostname']
            master_ip = self.client.master_info['address']['ip']
            master_port = self.client.master_info['address']['port']
        except Exception as e:
            self.logger.warn("Address not available from master: " + str(e))
        if master_address is None and master_ip is None:
            try:
                master_address = self.client.master_info['hostname']
                master_port = self.client.master_info['port']
            except Exception:
                self.logger.warn("Could not get master address info")

        if master_ip is not None and master_address is None:
            master_address = master_ip

        if master_address is not None:
            self.logger.info("Master hostname: " + str(master_address))
            self.logger.info("Master port: " + str(master_port))
            self.redis_handler.set(self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':master',
                                   master_address + ':' + str(master_port))

        self.redis_call(self.redis_handler.set,
            self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':frameworkId',
            self.frameworkId
        )
        self.redis_call(self.redis_handler.set,
            self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':streamId',
            self.driver.streamId
        )

        # Get slaves IP/port vs slave id
        self.slaves = {}
        slave_list = self.usage()
        self.logger.info("Load slaves information")
        for slave in slave_list:
            self.slaves[slave['id']] = slave['hostname']
        self.logger.debug(str(self.slaves))

        # Reconcile at startup
        if 'mesos' in self.cfg and 'reconcile' in self.cfg['mesos'] and self.cfg['mesos']['reconcile']:
            self.logger.info("Reconcile any running task")
            tasks = []
            for i in range(5):
                try:
                    tasks = self.jobs_handler.find({'status.primary': 'running'})
                    break
                except pymongo.errors.AutoReconnect:
                    self.logger.warn('Mongo:AutoReconnect')
                    time.sleep(pow(2, i))

            running_tasks = []
            for task in tasks:
                executor_requirements = task['requirements']['executor'].split(':')
                if self.get_id() != executor_requirements[0]:
                    self.logger.debug('task ' + str(task['id']) + ' not for this scheduler, skipping')
                    continue
                tid = task['id']
                if 'mesos-id' in task['container']['meta']:
                    tid = task['container']['meta']['mesos-id']
                task_status = {
                    'task_id': {
                        'value': str(tid)
                    },
                    'state': 'TASK_RUNNING'

                }
                self.logger.debug('Mesos:Reconcile:Task:' + str(task['id']))
                running_tasks.append(task_status)
            driver.reconcile(running_tasks)

    def failure_event(self, message):
        self.logger.error(
            "A failure event have been received: %s" % (str(message))
        )

    def rescind_event(self, message):
        self.logger.error(
            "A rescind event have been received for offer: %s" % (str(message))
        )

    def status_update(self, update):
        self.logger.debug(
            "Task %s is in state %s" % (update['status']['task_id']['value'], update['status']['state'])
        )
        if update['status']['state'] in ['TASK_LOST', 'TASK_ERROR', 'TASK_DROPPED', 'TASK_UNREACHABLE', 'TASK_GONE', 'TASK_GONE_BY_OPERATOR', 'TASK_UNKNOWN']:
            self.logger.warn(
                "Task %s is in state %s" % (update['status']['task_id']['value'], update['status']['state'])
            )
        tid = update['status']['task_id']['value'].split('-')
        if len(tid) > 1:
            jobid = tid[0]
        else:
            # Backward compatibility for old jobs (godocker <= 1.3.4)
            jobid = update['status']['task_id']['value']

        if update['status']['state'] in ['TASK_RUNNING', 'TASK_UNREACHABLE']:
            # Switched to RUNNING, get container id
            job = None
            for i in range(5):
                try:
                    job = self.jobs_handler.find_one({'id': int(jobid)})
                    break
                except pymongo.errors.AutoReconnect:
                    self.logger.warn('Mongo:AutoReconnect')
                    time.sleep(pow(2, i))

            # Switched to RUNNING, get container id
            containerId = None
            if self.cfg['mesos']['unified']:
                ip_address = None
                if 'container_status' in update['status'] and update['status']['container_status']:
                    for network_info in update['status']['container_status']['network_infos']:
                        for ip_address_info in network_info['ip_addresses']:
                            ip_address = ip_address_info['ip_address']
                # if using port mapping, use slave ip
                if self.cfg['mesos'].get('port_mapper_network_name', None):
                    ip_address = job['container']['meta']['Node']['Name']
                for i in range(5):
                    try:
                        self.jobs_handler.update(
                            {
                                'id': int(jobid)
                            },
                            {
                                '$set': {
                                    'container.ip_address': ip_address,
                                    'container.status': 'ready'
                                }
                            })
                        break
                    except pymongo.errors.AutoReconnect:
                        self.logger.warn('Mongo:AutoReconnect')
                        time.sleep(pow(2, i))

            else:
                try:
                    if 'data' in update['status'] and str(update['status']['data']) != "":
                        b64_data = base64.b64decode(update['status']['data'])
                        containers = json.loads(b64_data)
                        self.logger.debug('Mesos:Task:%s:data:%s' % (update['status']['task_id']['value'], b64_data))
                        containerId = containers[0]["Id"]
                        if self.network:
                            container_network_name = self.network.network(job['requirements']['network'], job['user'])
                            ip_address = containers[0]['NetworkSettings']['Networks'][container_network_name]['IPAddress']
                        else:
                            ip_address = job['container']['meta']['Node']['Name']
                        for i in range(5):
                            try:
                                self.jobs_handler.update(
                                    {
                                        'id': int(jobid)
                                    },
                                    {
                                        '$set': {
                                            'container.id': containerId,
                                            'container.ip_address': ip_address,
                                            'container.status': 'ready'
                                        }
                                    }
                                )
                                break
                            except pymongo.errors.AutoReconnect:
                                self.logger.warn('Mongo:AutoReconnect')
                                time.sleep(pow(2, i))

                except Exception as e:
                    self.logger.debug("Could not extract info from TaskStatus: " + str(e))
                    containerId = None

        if update['status']['state'] in ['TASK_FINISHED', 'TASK_FAILED', 'TASK_KILLED', 'TASK_LOST', 'TASK_ERROR', 'TASK_DROPPED', 'TASK_GONE', 'TASK_GONE_BY_OPERATOR']:
            self.logger.debug('Mesos:Task:Over:' + str(update['status']['task_id']['value']))
            job = None
            for i in range(5):
                try:
                    job = self.jobs_handler.find_one({'id': int(jobid)})
                    break
                except pymongo.errors.AutoReconnect:
                    self.logger.warn('Mongo:AutoReconnect')
                    time.sleep(pow(2, i))

            if job is not None and 'meta' in job['container'] and job['container']['meta'] is not None and 'Node' in job['container']['meta']:
                self.plugin_zfs_unmount(job['container']['meta']['Node']['Name'], job)

        if 'reason' in update['status'] and update['status']['reason']:
            self.redis_call(self.redis_handler.set,
                self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':over:' + str(jobid) + ':reason',
                update['status']['reason']
            )
        if 'message' in update['status'] and update['status']['message']:
            self.redis_call(self.redis_handler.set,
                self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':over:' + str(jobid) + ':reasonmsg',
                update['status']['message']
            )

        self.redis_call(self.redis_handler.set,
            self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':over:' + str(jobid),
            update['status']['state']
        )

    def offer_received(self, offers):
        '''
        Basic placement strategy (loop over offers and try to push as possible)
        '''
        self.logger.debug('OFFER RECEIVED: ' + str(offers))
        if self.Terminated:
            self.logger.info("Stop requested")
            return

        self.logger.debug('Mesos:Offers:Begin')
        # Get tasks
        tasks = []
        redis_task = self.redis_call(self.redis_handler.lpop, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':pending')
        while redis_task is not None:
            task = json.loads(redis_task)
            task['mesos_offer'] = False
            tasks.append(task)
            redis_task = self.redis_call(self.redis_handler.lpop, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':pending')

        for mesos_offer in offers:
            offer = mesos_offer.get_offer()
            # Check maintenance of node
            self.logger.debug(offer)
            if 'unavailability' in offer and offer['unavailability']['start']['nanoseconds']:
                self.logger.debug("Node %s in planned maintenance, skipping..." % (offer['hostname']))
                mesos_offer.decline()
                continue

            if not tasks:
                self.logger.debug('Mesos:Offer:Decline:NoTask')
                mesos_offer.decline()
                continue

            offer_tasks = []
            offerCpus = 0
            offerMem = 0
            offerGpus = 0
            labels = {}
            resources = {'cpus': {}, 'mem': {}, 'gpus': {}}
            for resource in offer['resources']:
                if resource['name'] == "cpus":
                    offerCpus += resource['scalar']['value']
                elif resource['name'] == "mem":
                    offerMem += resource['scalar']['value']
                elif resource['name'] == "gpus":
                    offerGpus += resource['scalar']['value']
                role = '*'
                if 'role' in resource:
                    role = resource['role']
                if resource['name'] in ['cpus', 'mem', 'gpus']:
                    resources[resource['name']][role] = resource['scalar']['value']
            offer['god_resources'] = resources

            if 'attributes' in offer:
                for attr in offer['attributes']:
                    if attr['type'] == 'TEXT':
                        labels[attr['name']] = attr['text']['value']
            self.logger.debug("Mesos:Labels:" + str(labels))
            if 'hostname' not in labels:
                if offer['agent_id']['value'] in self.slaves:
                    labels['hostname'] = self.slaves[offer['agent_id']['value']]
                    self.logger.debug('Mesos:GetSlaveIdFromMasterInfo:%s' % (labels['hostname']))
                elif 'hostname' in offer and offer['hostname']:
                    labels['hostname'] = offer['hostname']
                    self.logger.debug('Mesos:GetSlaveIdFromOffer:%s' % (labels['hostname']))
                else:
                    self.logger.error('Mesos:Error:Configuration: missing label hostname')

            self.logger.debug(
                "Mesos:Received offer %s with cpus: %s and mem: %s" % (offer['id']['value'], offerCpus, offerMem)
            )

            for task in tasks:
                if offerCpus == 0 or offerMem == 0:
                    break
                if 'gpus' not in task['requirements']:
                    task['requirements']['gpus'] = 0
                if not task['mesos_offer'] and task['requirements']['cpu'] <= offerCpus and task['requirements']['ram'] * 1000 <= offerMem and task['requirements']['gpus'] <= offerGpus:
                    # check for reservation constraints, if any
                    try:
                        self.logger.debug("Try to place task " + str(task['id']))
                        if 'hostname' in labels and 'skip_failed_nodes' in self.cfg['failure_policy'] and self.cfg['failure_policy']['skip_failed_nodes']:
                            if 'failure' in task['status'] and task['status']['failure']['nodes']:
                                if labels['hostname'] in task['status']['failure']['nodes']:
                                    # Task previsouly failed on this node, skip this node
                                    self.logger.debug("Task:" + str(task['id']) + ":Failure:Skip:" + labels['hostname'])
                                    continue
                        if 'reservation' in labels:
                            self.logger.debug("Node has reservation")
                            reservations = labels['reservation'].split(',')
                            offer_hostname = "undefined"
                            if 'hostname' in labels:
                                offer_hostname = labels['hostname']
                            self.logger.debug("Check reservation for " + offer_hostname)
                            if task['user']['id'] not in reservations and task['user']['project'] not in reservations:
                                self.logger.debug("User " + task['user']['id'] + " not allowed to execute on " + offer_hostname)
                                continue

                        if 'label' in task['requirements'] and task['requirements']['label']:
                            task['requirements']['resources'] = {}
                            for task_resource in task['requirements']['label']:
                                if not task_resource or not task_resource.startswith('resource=='):
                                    continue
                                requested_resource = task_resource.split('==')
                                requested_resource = requested_resource[1]
                                if requested_resource not in task['requirements']['resources']:
                                    task['requirements']['resources'][requested_resource] = 1
                                else:
                                    task['requirements']['resources'][requested_resource] += 1
                            # check if enough resources
                            has_enough_resources = True
                            for requested_resource in task['requirements']['resources']:
                                quantity = task['requirements']['resources'][requested_resource]
                                if not self.has_enough_resource(offer, requested_resource, quantity):
                                    self.logger.debug('Not enough ' + requested_resource + ' on this node')
                                    has_enough_resources = False
                                    break
                            if not has_enough_resources:
                                self.logger.debug("Not enough specific resources for task " + str(task['id']))
                                del task['requirements']['resources']
                                continue

                        # check for label constraints, if any
                        if 'label' in task['requirements'] and task['requirements']['label']:
                            is_ok = True
                            for req in task['requirements']['label']:
                                if not req:
                                    continue
                                reqlabel = req.split('==')
                                if reqlabel[0] == 'resource':
                                    continue

                                if reqlabel[0] not in labels or reqlabel[1] != labels[reqlabel[0]]:
                                    is_ok = False
                                    break
                            if not is_ok:
                                self.logger.debug("Label requirements do not match for task " + str(task['id']))
                                continue

                        (res, zfs_path) = self.plugin_zfs_mount(labels['hostname'], task)
                        if not res:
                            self.logger.debug("Zfs mount not possible for task " + str(task['id']))
                            continue
                        else:
                            if zfs_path is not None:
                                task['requirements']['tmpstorage']['path'] = zfs_path
                            else:
                                if task['requirements']['tmpstorage'] is not None:
                                    task['requirements']['tmpstorage']['path'] = None
                                else:
                                    task['requirements']['tmpstorage'] = {'path': None, 'size': ''}

                        new_task = self.new_task(offer, task, labels)

                    except Exception as e:
                        self.logger.exception("Error with task " + str(task['id']) + ": " + str(e))
                        task['status']['reason'] = 'Invalid task'
                        # An error occur, switch to next task
                        continue
                    if new_task is None:
                        self.logger.debug('Mesos:Task:Error:Failed to create new task ' + str(task['id']))
                        continue
                    offer_tasks.append(new_task)
                    offerCpus -= task['requirements']['cpu']
                    offerMem -= task['requirements']['ram'] * 1000
                    offerGpus -= task['requirements']['gpus']
                    task['mesos_offer'] = True
                    self.logger.debug('Mesos:Task:Running:' + str(task['id']))
                    self.redis_call(self.redis_handler.rpush, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':running', dumps(task))
                    self.redis_call(self.redis_handler.set, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':over:' + str(task['id']), 0)
            if not offer_tasks:
                mesos_offer.decline()
            else:
                self.logger.debug("Accept tasks: " + str(offer_tasks))
                mesos_offer.accept(offer_tasks)

        for task in tasks:
            if not task['mesos_offer']:
                self.logger.debug('Mesos:Task:Rejected:' + str(task['id']))
                self.redis_call(self.redis_handler.rpush, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':rejected', dumps(task))

        if tasks:
            self.redis_call(self.redis_handler.set, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':offer', 1)
        self.logger.debug('Mesos:Offers:End')

    def __get_resources_from_offer(self, task, resource, requirement, offer):
        roles = ['*']
        additional_role = self.__get_role()
        if additional_role:
            roles = [additional_role, '*']
        self.logger.debug('Get resource for roles ' + str(roles))

        do_stop = False
        for role in roles:
            if do_stop:
                break
            if role not in offer['god_resources'][resource]:
                continue
            role_resource = offer['god_resources'][resource][role]
            tmp_resource = None
            if role_resource >= requirement:
                offer['god_resources'][resource][role] -= requirement
                tmp_resource = {
                    'name': resource,
                    'type': 'SCALAR',
                    'scalar': {'value': requirement}
                }
                do_stop = True
            else:
                requirement -= offer['god_resources'][resource][role]
                tmp_requirement = offer['god_resources'][resource][role]
                offer['god_resources'][resource][role] = 0
                tmp_resource = {
                    'name': resource,
                    'type': 'SCALAR',
                    'scalar': {'value': tmp_requirement}
                }
            if tmp_resource:
                tmp_resource['role'] = role
                if 'allocation_info' in offer and offer['allocation_info']:
                    tmp_resource['allocation_info'] = offer['allocation_info']
                self.logger.debug('Reserve resource: ' + str(tmp_resource))
                task['resources'].append(tmp_resource)

    def new_task(self, offer, job, labels=None):
        '''
        Creates a task for mesos
        '''
        # in mesos.proto slave_id, in http lib slave_id => agent_id
        if 'slave_id' not in offer:
            offer['slave_id'] = {'value': offer['agent_id']['value']}

        task = {
            'name': None,
            'task_id': None,
            'agent_id': None,
            'resources': [],
            'labels': {'labels': [
                {'key': 'project', 'value': job['user']['project']},
                {'key': 'interactive', 'value': str(job['command'].get('interactive', False))}
            ]}
        }

        container = {
            'type': 'DOCKER',
            'volumes': []
        }

        if self.cfg['mesos']['unified']:
            container['type'] = 'MESOS'

        # Reserve requested resource and mount related volumes
        if 'resources' in job['requirements'] and job['requirements']['resources']:
            for requested_resource in job['requirements']['resources']:
                quantity = job['requirements']['resources'][requested_resource]
                mesos_resources = {
                    'name': requested_resource,
                    'type': 'RANGES',
                    'ranges': {'range': []}
                }

                for resource in offer['resources']:
                    if resource['name'] == requested_resource and quantity > 0:
                        for mesos_range in resource['ranges']['range']:
                            if mesos_range['begin'] <= mesos_range['end']:
                                if (1 + mesos_range['end'] - mesos_range['begin'] >= quantity):
                                    # take what is necessary
                                    mesos_resource = {
                                        'begin': mesos_range['begin'],
                                        'end': mesos_range['begin'] + quantity - 1
                                    }
                                    mesos_resources['ranges']['range'].append(mesos_resource)
                                    mesos_range['begin'] = quantity
                                    quantity = 0
                                else:
                                    # take what is available
                                    mesos_resource = {
                                        'begin': mesos_range['begin'],
                                        'end': mesos_range['begin'] + (mesos_range['end'] - mesos_range['begin'])
                                    }
                                    mesos_resources['ranges']['range'].append(mesos_resource)
                                    mesos_range['begin'] += 1 + mesos_range['end'] - mesos_range['begin']
                                    quantity -= 1 + mesos_range['end'] - mesos_range['begin']
                                self.logger.debug("Take resources: " + str(mesos_resource['begin']) + "-" + str(mesos_resource['end']))
                task['resources'].append(mesos_resources)
            for mesos_range in mesos_resources['ranges']['range']:
                for res_range in range(mesos_range['begin'], mesos_range['end'] + 1):
                    resource_volume_id = mesos_resources['name'] + "_" + str(res_range)
                    self.logger.debug("Add volumes for resource " + resource_volume_id)
                    if resource_volume_id in labels:
                        resource_volumes = labels[resource_volume_id].split(',')
                        for resource_volume in resource_volumes:
                            volume = {
                                'container_path': resource_volume,
                                'host_path': resource_volume,
                                'mode': 'RW'
                            }
                            container['volumes'].append(volume)
                            self.logger.debug("Add volume for resource: " + resource_volume)
            del job['requirements']['resources']

        dockercfg = None

        for v in job['container']['volumes']:
            if v['mount'] is None:
                v['mount'] = v['path']

            volume = {
                'container_path': v['mount'],
                'host_path': v['path'],
                'mode': 'RO'
            }
            if 'acl' in v and v['acl'] == 'rw':
                volume['mode'] = 'RW'  # mesos_pb2.Volume.Mode.RW
            else:
                volume['mode'] = 'RO'  # mesos_pb2.Volume.Mode.RO

            container['volumes'].append(volume)

            if v['name'] == 'home':
                if os.path.exists(os.path.join(v['path'], 'docker.tar.gz')):
                    self.logger.debug('Add .dockercfg ' + os.path.join(v['path'], 'docker.tar.gz'))
                    dockercfg = os.path.join(v['path'], 'docker.tar.gz')

        if job['requirements']['tmpstorage']['path'] is not None:
            volume = {
                'container_path': '/tmp-data',
                'host_path': job['requirements']['tmpstorage']['path'],
                'mode': 'RO'
            }
            container['volumes'].append(volume)

        if 'meta' not in job['container'] or job['container']['meta'] is None:
            job['container']['meta'] = {}
        if 'Node' not in job['container']['meta'] or job['container']['meta']['Node'] is None:
            job['container']['meta']['Node'] = {}

        tid = str(job['id'])
        tcount = '-0'
        if 'failure' in job['status'] and 'count' in job['status']['failure']:
            tcount = '-' + str(job['status']['failure']['count'])

        tid += tcount
        job['container']['meta']['mesos-id'] = tid

        command = {
            'value': job['command']['script'],
            'uris': []
        }
        if dockercfg:
            command['uris'].append({'value': dockercfg})

        task['command'] = command
        task['task_id'] = {'value': tid}
        task['agent_id'] = {'value': offer['slave_id']['value']}
        task['name'] = '[' + job['user']['project'] + '] ' + job['meta']['name']

        self.__get_resources_from_offer(task, 'cpus', job['requirements']['cpu'], offer)
        self.__get_resources_from_offer(task, 'mem', job['requirements']['ram'] * 1000, offer)
        if 'gpus' in job['requirements'] and job['requirements']['gpus'] > 0:
            self.__get_resources_from_offer(task, 'gpus', job['requirements']['gpus'], offer)

        job['container']['meta']['Node']['slave'] = offer['slave_id']['value']
        job['container']['meta']['offer'] = offer['id']['value']
        if labels is not None and 'hostname' in labels:
            job['container']['meta']['Node']['Name'] = labels['hostname']
        else:
            job['container']['meta']['Node']['Name'] = offer['slave_id']['value']
        self.logger.debug("Task placed on host " + str(job['container']['meta']['Node']['Name']))

        port_list = []
        job['container']['port_mapping'] = []
        job['container']['ports'] = []
        if 'ports' in job['requirements']:
            port_list = job['requirements']['ports']
        if job['command']['interactive']:
            port_list.append(22)

        if self.cfg['mesos']['unified']:
            # Host mode only for the moment
            # => no port mapping or possible conflict
            # Need Calico or equivalent to have per task IP and network isolation
            # More doc:
            # https://github.com/projectcalico/calico-containers/blob/v0.19.0/README.md
            # http://mesos.apache.org/documentation/latest/networking-for-mesos-managed-containers/

            task['command']['shell'] = True
            docker = {
                'image': {
                    'type': 'DOCKER',
                    'docker': {'name': job['container']['image']}
                }
            }
            if self.cfg['mesos']['force_pull_image']:
                docker['image']['cached'] = False
            # Request an IP from a network module
            if self.network:
                container_network_name = self.network.network(job['requirements']['network'])
                self.network.create_network(container_network_name)
                container['network_infos'] = [{
                    'name': container_network_name,
                    'ip_addresses': [
                        {
                            'protocol': 'IPv4'
                        }
                    ]
                }]
            else:
                if port_list and self.cfg['mesos'].get('port_mapper_network_name', None):
                    mesos_ports = {
                        'name': 'ports',
                        'type': 'RANGES',
                        'ranges': {'range': []}
                    }
                    for port in port_list:
                        if self.cfg['port_allocate']:
                            mapped_port = self.get_mapping_port(offer, job)
                            if mapped_port is None:
                                return None
                        else:
                            mapped_port = port
                        job['container']['port_mapping'].append({'host': mapped_port, 'container': port})
                        mesos_ports['ranges']['range'].append({
                            'begin': mapped_port,
                            'end': mapped_port
                        })
                    if job['container'].get('port_mapping', None):
                        container['network_infos'] = [{
                            'name': self.cfg['mesos']['port_mapper_network_name'],
                            'ip_addresses': [
                                {
                                    'protocol': 'IPv4'
                                }
                            ],
                            'port_mappings': []
                        }]
                    for port_mapped in job['container']['port_mapping']:
                        container['network_infos'][0]['port_mappings'].append({
                            'host_port': port_mapped['host'],
                            'container_port': port_mapped['container']
                        })
                    task['resources'].append(mesos_ports)

            container['mesos'] = docker
        else:
            # Docker containerizer
            docker = {
                'image': job['container']['image'],
                'network': 'BRIDGE',
                'force_pull_image': self.cfg['mesos']['force_pull_image'],
                'parameters': [
                    {
                        'key': 'user',
                        'value': 'root'
                    }
                ],
                'port_mappings': []
            }

            if self.network:
                # Define the specific network to use
                docker.network = 4
                container_network_name = self.network.network(job['requirements']['network'])
                self.network.create_network(container_network_name)
                container['network_infos'] = {
                    'name': container_network_name
                }
            else:
                if port_list:
                    mesos_ports = {
                        'name': 'ports',
                        'type': 'RANGES',
                        'ranges': {'range': []}
                    }
                    for port in port_list:
                        if self.cfg['port_allocate']:
                            mapped_port = self.get_mapping_port(offer, job)
                            if mapped_port is None:
                                return None
                        else:
                            mapped_port = port
                        job['container']['port_mapping'].append({'host': mapped_port, 'container': port})

                        docker['port_mappings'].append({
                            'host_port': mapped_port,
                            'container_port': port
                        })
                        mesos_ports['ranges']['range'].append({
                            'begin': mapped_port,
                            'end': mapped_port
                        })

                    task['resources'].append(mesos_ports)
            container['docker'] = docker
        task['container'] = container
        # Add port in env variables
        if job['container']['port_mapping']:
            task['command']['environment'] = {'variables': []}
            for env_port in job['container']['port_mapping']:
                task['command']['environment']['variables'].append({
                    'name': 'PORT_' + str(env_port['container']),
                    'value': str(env_port['host'])
                })
                if 'name' in env_port:
                    task['command']['environment']['variables'].append({
                        'name': 'PORT_' + env_port['name'],
                        'value': str(env_port['host'])
                    })
        return task

    def disconnected(self, message):
        '''
        Mesos was disconnected
        '''
        self.logger.error('Mesos:Event:Disconnected:' + str(message))
        self.mesos_connected = False
        self.logger.info('Mesos:WaitForReconnect')

    def reconnected(self, message):
        '''
        Mesos was reconnected
        '''
        self.logger.warn('Mesos:Event:Reconnected:' + str(message))
        self.mesos_connected = True

    def __get_role(self):
        role = None
        if 'role' in self.cfg['mesos'] and self.cfg['mesos']['role']:
            role = self.cfg['mesos']['role']
        # Allow specific framework role definition
        if self.cfg.get(self.get_id(), None) and self.cfg[self.get_id()].get('role', None):
            role = self.cfg[self.get_id()]['role']
        return role

    def open(self, proc_type):
        '''
        Request start of executor if needed
        '''
        self.th = None
        if proc_type is not None and proc_type == 1:
            # do not start framework on watchers
            return

        self.driver = None
        frameworkId = self.redis_call(self.redis_handler.get, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':frameworkId')
        f_name = "Go-Docker"
        if 'name' in self.cfg['mesos'] and self.cfg['mesos']['name']:
            f_name = self.cfg['mesos']['name']
        webui = self.cfg.get('web_endpoint', '')

        if frameworkId:
            # Reuse previous framework identifier
            self.logger.info("Mesos:FrameworkId:" + str(frameworkId))
            self.client = MesosClient([self.cfg['mesos']['master']], frameworkName=f_name, frameworkId=frameworkId, frameworkWebUI=webui)
        else:
            self.logger.info("Mesos:FrameworkId:None, registering as a new framework")
            self.client = MesosClient([self.cfg['mesos']['master']], frameworkName=f_name, frameworkWebUI=webui)

        role = self.__get_role()
        if role:
            self.client.set_role(role)
            self.logger.info('Register with role ' + role)

        if os.getenv("MESOS_AUTHENTICATE"):
            self.client.set_credentials(os.getenv("DEFAULT_PRINCIPAL"), os.getenv("DEFAULT_SECRET"))

        self.client.set_failover_timeout(3600 * 24 * 7)

        if self.cfg['mesos']['native_gpu']:
            self.client.add_capability('GPU_RESOURCES')

        if os.getenv("MESOS_CHECKPOINT"):
            self.client.set_checkpoint(True)

        self.client.on(MesosClient.SUBSCRIBED, self.subscribed)
        self.client.on(MesosClient.OFFERS, self.offer_received)
        self.client.on(MesosClient.UPDATE, self.status_update)
        self.client.on(MesosClient.FAILURE, self.failure_event)
        self.client.on(MesosClient.RESCIND, self.rescind_event)
        self.client.on(MesosClient.DISCONNECTED, self.disconnected)
        self.client.on(MesosClient.RECONNECTED, self.reconnected)
        self.th = MesosFramework(self.client)
        self.th.start()

    def reset(self):
        '''
        Reset frameworkId
        '''
        self.redis_call(self.redis_handler.delete, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':frameworkId')
        self.redis_call(self.redis_handler.delete, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':master')
        self.redis_call(self.redis_handler.delete, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':streamId')

    def close(self):
        '''
        Request end of executor if needed
        '''
        if self.th is not None:
            self.th.client.disconnect_framework()
        #    self.driver.tearDown()
        self.Terminated = True

    def run_all_tasks(self, tasks, callback=None):
        '''
        Execute all task list on executor system, all tasks must be executed together

        NOT IMPLEMENTED, will reject all tasks

        :param tasks: list of tasks to run
        :type tasks: list
        :param callback: callback function to update tasks status (running/rejected)
        :type callback: func(running list,rejected list)
        :return: tuple of submitted and rejected/errored tasks
        '''
        self.logger.error('run_all_tasks not implemented')
        return ([], tasks)

    def run_tasks(self, tasks, callback=None):
        '''
        Execute task list on executor system

        :param tasks: list of tasks to run
        :type tasks: list
        :param callback: callback function to update tasks status (running/rejected)
        :type callback: func(running list,rejected list)
        :return: tuple of submitted and rejected/errored tasks
        '''
        if not self.mesos_connected:
            self.logger.warn('Mesos:NotConnected:WaitingForReconnection')
            return ([], tasks)
        # Add tasks in redis to be managed by mesos
        self.redis_call(self.redis_handler.set, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':offer', 0)
        for task in tasks:
            self.redis_call(self.redis_handler.rpush, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':pending', dumps(task))
        # Wait for offer receival and treatment
        self.logger.debug('Mesos:WaitForOffer:Begin')
        mesos_offer = int(self.redis_call(self.redis_handler.get, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':offer'))
        while mesos_offer != 1 and not self.Terminated:
            if not self.mesos_connected:
                self.logger.warn('Mesos:NotConnected:WaitingForReconnection')
                return ([], tasks)
            self.logger.debug('Mesos:WaitForOffer:Wait')
            time.sleep(1)
            mesos_offer = int(self.redis_call(self.redis_handler.get, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':offer'))
        self.logger.debug('Mesos:WaitForOffer:End')
        # Get result
        rejected_tasks = []
        running_tasks = []
        redis_task = self.redis_call(self.redis_handler.lpop, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':running')
        while redis_task is not None:
            task = json.loads(redis_task)
            task['container']['status'] = 'initializing'
            running_tasks.append(task)
            redis_task = self.redis_call(self.redis_handler.lpop, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':running')
        redis_task = self.redis_call(self.redis_handler.lpop, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':rejected')
        while redis_task is not None:
            task = json.loads(redis_task)
            rejected_tasks.append(task)
            redis_task = self.redis_call(self.redis_handler.lpop, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':rejected')

        return (running_tasks, rejected_tasks)

    def watch_tasks(self, task):
        '''
        Get task status

        Must update task with following params if task is over:
            task['container']['meta']['State']['ExitCode']
        In case of node failure:
            task['status']['failure'] = { 'reason': reason_of_failure,
                                           'nodes': [ node(s)_where_failure_occured]}


        :param task: current task
        :type task: Task
        :param over: is task over
        :type over: bool
        '''
        self.logger.debug('Mesos:Task:Check:Running:' + str(task['id']))
        mesos_task = self.redis_call(self.redis_handler.get, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':over:' + str(task['id']))
        if mesos_task is not None and mesos_task in ['TASK_FINISHED', 'TASK_FAILED', 'TASK_KILLED', 'TASK_LOST', 'TASK_ERROR', 'TASK_GONE', 'TASK_GONE_BY_OPERATOR', 'TASK_DROPPED']:
            self.logger.debug('Mesos:Task:Check:IsOver:' + str(task['id']))
            exit_code = mesos_task
            if 'State' not in task['container']['meta']:
                task['container']['meta']['State'] = {}
            if exit_code == 'TASK_FINISHED':
                task['container']['meta']['State']['ExitCode'] = 0
            elif exit_code == 'TASK_KILLED':
                task['container']['meta']['State']['ExitCode'] = 137
            else:
                task['container']['meta']['State']['ExitCode'] = 1
            task['status']['reason'] = None
            if mesos_task in ['TASK_FAILED', 'TASK_LOST', 'TASK_ERROR', 'TASK_DROPPED', 'TASK_GONE', 'TASK_GONE_BY_OPERATOR']:
                reason = self.redis_call(self.redis_handler.get, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':over:' + str(task['id']) + ':reason')
                if not reason:
                    reason = None
                if reason is None and mesos_task == 'TASK_FAILED':
                    task['status']['reason'] = 'Task did not succeed'
                else:
                    task['status']['reason'] = 'System crashed or failed to start the task: '
                reason_msg = self.redis_call(self.redis_handler.get, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':over:' + str(task['id']) + ':reasonmsg')
                if reason_msg:
                    task['status']['reason'] += ': ' + reason_msg

                node_name = None
                if 'Node' in task['container']['meta'] and 'Name' in task['container']['meta']['Node']:
                    node_name = task['container']['meta']['Node']['Name']
                if 'failure' not in task['status']:
                    task['status']['failure'] = {'reason': reason, 'nodes': [], 'info': mesos_task}
                else:
                    task['status']['failure']['reason'] = task['status']['reason']

                if node_name:
                    task['status']['failure']['nodes'].append(node_name)

            self.redis_call(self.redis_handler.delete, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':over:' + str(task['id']))
            self.redis_call(self.redis_handler.delete, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':over:' + str(task['id']) + ':reason')
            self.redis_call(self.redis_handler.delete, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':over:' + str(task['id']) + ':reasonmsg')

            return (task, True)
        else:
            self.logger.debug('Mesos:Task:Check:IsRunning:' + str(task['id']))
            self.get_container_id(task)
            return (task, False)

    def get_container_id(self, task):
        '''
        Extract container id from slave
        '''
        if 'id' not in task['container']:
            task['container']['id'] = None

        if task['container']['id']:
            return

        if self.cfg['mesos']['unified']:
            http = urllib3.PoolManager()
            r = None
            try:
                # r = http.urlopen('GET', 'http://' + task['container']['meta']['Node']['Name'] + ':5051/containers/' + str(task['container']['meta']['mesos-id']))
                r = http.urlopen('GET', 'http://' + task['container']['meta']['Node']['Name'] + ':5051/containers')
                if r.status == 200:
                    containers = []
                    if version_info[0] < 3:
                        containers = json.loads(r.data)
                    else:
                        containers = json.loads(r.data.decode('UTF-8'))
                    for container in containers:
                        if container['executor_id'] == str(task['container']['meta']['mesos-id']):
                            task['container']['id'] = container['container_id']
                            break
                    for i in range(5):
                        try:
                            self.jobs_handler.update({'id': task['id']}, {'$set': {'container.id': task['container']['id']}})
                            break
                        except pymongo.errors.AutoReconnect:
                            self.logger.warn('Mongo:AutoReconnect')
                            time.sleep(pow(2, i))

            except Exception as e:
                self.logger.error('Could not get container identifier: ' + str(e))

        # Mesos <= 0.22, container id is not in TaskStatus, let's query mesos
        '''
        if containerId is None:
            http = urllib3.PoolManager()
            r = None
            try:
                r = http.urlopen('GET', 'http://' + job['container']['meta']['Node']['Name'] + ':5051/slave(1)/state.json')
                    if r.status == 200:
                    slave = json.loads(r.data)
                    for f in slave['frameworks']:
                        if f['name'] == "Go-Docker Mesos":
                            for executor in f['executors']:
                                if str(executor['id']) == str(update.task_id.value):
                                    task['container']['id'] = 'mesos-' + executor['container']
                                    self.jobs_handler.update({'id': int(update.task_id.value)}, {'$set': {'container.id': task['container']['id']}})
                                    break
                            break
            except Exception as e:
                self.logger.error('Failed to contact mesos slave: ' + str(e))
        '''

    def kill_task(self, task):
        '''
        Kills a running task

        :param tasks: task to kill
        :type tasks: Task
        :return: (Task, over) over is True if task could be killed
        '''
        self.logger.debug('Mesos:Task:Kill:Check:' + str(task['id']))
        mesos_task = self.redis_call(self.redis_handler.get, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':over:' + str(task['id']))
        if mesos_task is not None and mesos_task in ['TASK_FINISHED', 'TASK_FAILED', 'TASK_KILLED', 'TASK_LOST', 'TASK_ERROR', 'TASK_GONE']:
            self.logger.debug('Mesos:Task:Kill:IsOver:' + str(task['id']))
            exit_code = 137
            if 'State' not in task['container']['meta']:
                task['container']['meta']['State'] = {}
            if exit_code == 'TASK_FINISHED':
                task['container']['meta']['State']['ExitCode'] = 0
            elif exit_code == 'TASK_KILLED':
                task['container']['meta']['State']['ExitCode'] = 137
            else:
                task['container']['meta']['State']['ExitCode'] = 1
            self.redis_call(self.redis_handler.delete, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':over:' + str(task['id']))
            return (task, True)
        else:
            self.logger.debug('Mesos:Task:Kill:IsRunning:' + str(task['id']))
            frameworkId = self.redis_call(self.redis_handler.get, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':frameworkId')
            streamId = self.redis_call(self.redis_handler.get, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':streamId')
            mesos_master = self.redis_call(self.redis_handler.get, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':master')
            driver = MesosClient.SchedulerDriver(
                'http://' + mesos_master,
                frameworkId=frameworkId,
                streamId=streamId
            )
            try:
                if 'Node' in task['container']['meta'] and 'slave' in task['container']['meta']['Node']:
                    if 'mesos-id' in task['container']['meta']:
                        driver.kill(task['container']['meta']['Node']['slave'], str(task['container']['meta']['mesos-id']))
                    else:
                        # Backward compatiblity
                        driver.kill(task['container']['meta']['Node']['slave'], str(task['id']))
            except Exception as e:
                self.logger.exception('Mesos:Task:Kill:Error:' + str(e))
                return (task, False)
            return (task, None)

        return (task, True)

    def suspend_task(self, task):
        '''
        Suspend/pause a task

        :param tasks: task to suspend
        :type tasks: Task
        :return: (Task, over) over is True if task could be suspended
        '''
        self.logger.error('Not supported')
        return (task, False)

    def resume_task(self, task):
        '''
        Resume/restart a task

        :param tasks: task to resumed
        :type tasks: Task
        :return: (Task, over) over is True if task could be resumed
        '''
        self.logger.error('Not supported')
        return (task, False)

    def usage(self):
        '''
        Get resource usage

        :return: array of nodes with used/total resources with
            {
                'name': slave_hostname,
                'cpu': (cpus_used, cpu_total),
                'mem': (mem_used, mem_total),
                'gpus': (gpus_used, gpus_total)
                'disk': (disk_used, disk_total),
            }

        '''
        mesos_master = self.redis_call(self.redis_handler.get, self.cfg['redis_prefix'] + ':mesos:' + self.prefix + ':master')
        if not mesos_master:
            return []

        http = urllib3.PoolManager()
        r = http.urlopen('GET', 'http://' + mesos_master + '/master/state.json')
        master = {'slaves': []}
        if version_info[0] < 3:
            master = json.loads(r.data)
        else:
            master = json.loads(r.data.decode('UTF-8'))

        slaves = []
        for slave in master['slaves']:
            if slave['active']:
                if 'port' not in slave:
                    slave['port'] = 5051
                r = None
                try:
                    r = http.urlopen('GET', 'http://' + slave['hostname'] + ':' + str(slave['port']) + '/metrics/snapshot')
                except Exception as e:
                    self.logger.exception('Failed to connect to mesos slave %s: %s' % (slave['hostname'], str(e)))
                    continue
                state = {}
                if version_info[0] < 3:
                    state = json.loads(r.data)
                else:
                    state = json.loads(r.data.decode('UTF-8'))

                if 'slave/gpus_total' not in state:
                    state['slave/gpus_total'] = 0
                    state['slave/gpus_used'] = 0
                slaves.append({
                    'name': slave['hostname'],
                    'hostname': slave['hostname'],
                    'id': slave['id'],
                    'cpu': (int(state['slave/cpus_used']), int(state['slave/cpus_total'])),
                    'gpus': (int(state['slave/gpus_used']), int(state['slave/gpus_total'])),
                    'mem': (int(state['slave/mem_used']), int(state['slave/mem_total'])),
                    'disk': (int(state['slave/disk_used']), int(state['slave/disk_total']))
                })
        return slaves
