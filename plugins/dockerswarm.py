'''
Executor for the Docker Swarm (integrated with Docker)

This plugins needs the *docker* python module, and not *docker-py* module.
Both cannot be installed, if docker-py was installed, uninstall first then install docker package.

'''
from godocker.iExecutorPlugin import IExecutorPlugin
import datetime
# import iso8601
from godocker.utils import is_array_task

import docker
import pymongo
import sys
import time


class DockerSwarm(IExecutorPlugin):

    def get_name(self):
        return "dockerswarm"

    def get_type(self):
        return "Executor"

    def features(self):
        '''
        Get supported features

        :return: list of features within ['kill', 'cni']
        '''
        return ['kill', 'cni', 'interactive']

    def release_port(self, task):
        '''
        Release a port mapping
        '''
        for port in task['container']['ports']:
            host = 'godocker-swarm'
            self.logger.debug('Port:Back:' + host + ':' + str(port))
            self.redis_call(self.redis_handler.rpush,
                self.cfg['redis_prefix'] + ':ports:' + host,
                port
            )

    def get_mapping_port(self, host, task):
        '''
        Get a port mapping for interactive tasks

        If None is returned, task should rejected.

        :param host: hostname of the container
        :type host: str
        :param task: task
        :type task: int
        :return: available port, None if no port is available
        '''
        host = 'godocker-swarm'
        if not self.redis_call(self.redis_handler.exists, self.cfg['redis_prefix'] + ':ports:' + host):
            for i in range(self.cfg['port_range']):
                self.redis_handler.rpush(
                    self.cfg['redis_prefix'] + ':ports:' + host,
                    self.cfg['port_start'] + i
                )
        port = self.redis_call(self.redis_handler.lpop,
            self.cfg['redis_prefix'] + ':ports:' + host
        )
        if port is None:
            return None
        self.logger.debug('Port:Give:' + host + ':' + str(port))
        if 'ports' not in task['container']:
            task['container']['ports'] = []
        task['container']['ports'].append(port)
        return int(port)

    def set_config(self, cfg):
        self.cfg = cfg

        try:
            from docker import DockerClient, tls
        except Exception as e:
            self.logger.error('Failed to import docker module:' + str(e))
            self.logger.warn('dockerswarm plugin needs docker module and not docker-py, uninstall docker-py then install docker module')
            sys.exit(1)

        # base_url = None
        if 'docker' in self.cfg:
            tls_config = False
            if self.cfg['docker']['tls']:
                # https://docker-py.readthedocs.io/en/stable/tls/
                # Authenticate server based on public/default CA pool
                if not self.cfg['docker']['ca_cert'] and not self.cfg['docker']['client_cert']:
                    tls_config = tls.TLSConfig(verify=False)
                # Authenticate server based on given CA
                if self.cfg['docker']['ca_cert'] and not self.cfg['docker']['client_cert']:
                    tls_config = tls.TLSConfig(
                        ca_cert=self.cfg['docker']['ca_cert']
                        )
                # Authenticate with client certificate, do not authenticate server based on given CA
                if not self.cfg['docker']['ca_cert'] and self.cfg['docker']['client_cert']:
                    tls_config = tls.TLSConfig(
                        client_cert=(
                            self.cfg['docker']['client_cert'],
                            self.cfg['docker']['client_key']
                        ),
                        verify=False
                        )
                # Authenticate with client certificate, authenticate server based on given CA
                if self.cfg['docker']['ca_cert'] and self.cfg['docker']['client_cert']:
                    tls_config = tls.TLSConfig(
                        client_cert=(
                            self.cfg['docker']['client_cert'],
                            self.cfg['docker']['client_key']
                        ),
                        verify=self.cfg['docker']['ca_cert']
                        )
            self.docker_client = DockerClient(
                base_url=self.cfg['docker']['url'],
                version=self.cfg['docker']['api_version'],
                tls=tls_config
            )

        else:
            self.docker_client = DockerClient(
                base_url=self.cfg['docker_url'],
                version=self.cfg['docker_api_version']
            )

    def run_all_tasks(self, tasks, callback=None):
        '''
        Execute all task list on executor system, all tasks must be executed together

        NOT IMPLEMENTED, will reject all tasks

        :param tasks: list of tasks to run
        :type tasks: list
        :param callback: callback function to update tasks status (running/rejected)
        :type callback: func(running list,rejected list)
        :return: tuple of submitted and rejected/errored tasks
        '''
        self.logger.error('run_all_tasks not implemented')
        return ([], tasks)

    def run_tasks(self, tasks, callback=None):
        '''
        Execute task list on executor system

        :param tasks: list of tasks to run
        :type tasks: list
        :param callback: callback function to update tasks status (running/rejected)
        :type callback: func(running list,rejected list)
        :return: tuple of submitted and rejected/errored tasks
        '''
        running_tasks = []
        error_tasks = []
        for task in tasks:
            if is_array_task(task):
                # Virtual task for a task array, do not really execute
                running_tasks.append(task)
                self.logger.debug('Execute:Job:' + str(task['id']) + ':Skip:Array')
                continue
            # container = None
            service = None
            try:
                job = task
                port_list = []
                if 'ports' in job['requirements']:
                    port_list = job['requirements']['ports']
                if job['command']['interactive']:
                    port_list.append(22)

                # self.logger.warn('Reservation: '+str(job['requirements']['cpu'])+','+str(job['requirements']['ram'])+'g')
                vol_list = []
                for v in job['container']['volumes']:
                    if v['mount'] is None:
                        v['mount'] = v['path']
                    vol_list.append(v['mount'])

                constraints = []
                if 'label' in job['requirements'] and job['requirements']['label']:
                    for label in job['requirements']['label']:
                        constraints.append('constraint:' + label)

                volumes = []
                vol_binds = {}
                for v in job['container']['volumes']:
                    if v['mount'] is None:
                        v['mount'] = v['path']
                    mode = 'ro'
                    if 'acl' in v and v['acl'] == 'rw':
                        mode = 'rw'
                    vol_binds[v['path']] = {
                        'bind': v['mount'],
                        'mode': mode
                    }
                    volumes.append(v['path'] + ':' + v['mount'] + ':' + mode)

                job['container']['port_mapping'] = []

                # networking_config = None
                container_network_name = None

                endpointSpec = None
                networks = None
                if self.network is None:
                    port_mapping = {}
                    for port in port_list:
                        if self.cfg['port_allocate']:
                            mapped_port = self.get_mapping_port('godocker-swarm', job)
                            if mapped_port is None:
                                raise Exception('no port available')
                        else:
                            mapped_port = port
                        job['container']['port_mapping'].append({'host': mapped_port, 'container': port})
                        port_mapping[mapped_port] = port
                    endpointSpec = docker.types.EndpointSpec(ports=port_mapping)
                else:
                    container_network_name = self.network.network(job['requirements']['network'], job['user'])
                    network_status = self.network.create_network(container_network_name)
                    if not network_status:
                        raise Exception('Failed to create network %s for %s' % (job['requirements']['network'], container_network_name))
                    networks = [container_network_name]

                resources = docker.types.Resources(
                    cpu_reservation=1000000000 * job['requirements']['cpu'],
                    cpu_limit=1000000000 * job['requirements']['cpu'],
                    mem_reservation=1000000000 * job['requirements']['ram'],
                    mem_limit=1000000000 * job['requirements']['ram'],
                )
                policy = docker.types.RestartPolicy(condition="none")
                service = self.docker_client.services.create(
                    image=job['container']['image'],
                    command=[job['command']['script']],
                    resources=resources,
                    restart_policy=policy,
                    endpoint_spec=endpointSpec,
                    networks=networks,
                    user='root',
                    mounts=volumes,
                    constraints=constraints
                )
                self.logger.debug('Service:' + str(service.attrs))
                if service is None:
                    self.logger.error("Failed to create service for task %d" % task['id'])
                    error_tasks.append(job)
                    continue

                if 'meta' not in job['container'] or job['container']['meta'] is None:
                    job['container']['meta'] = {}
                job['container']['meta']['service_id'] = service.id
                job['container']['id'] = None

                job['status']['reason'] = None
                job['status']['override'] = True
                job['status']['secondary'] = 'pending swarm'
                if job['container']['meta'] is None:
                    job['container']['meta'] = {}
                if 'Node' not in job['container']['meta']:
                    job['container']['meta']['Node'] = {'Name': 'unknown'}

                running_tasks.append(job)
                self.logger.debug(
                    'Execute:Job:' + str(job['id'])
                )
            except Exception as e:
                self.logger.exception('Execute:Job:' + str(job['id']) + ':' + str(e))
                job['status']['reason'] = str(e)
                if job['container']['meta'] is None:
                    job['container']['meta'] = {}
                if 'Node' not in job['container']['meta']:
                    job['container']['meta']['Node'] = {'Name': 'unknown'}
                error_tasks.append(job)
                if service:
                    service.remove()
        return (running_tasks, error_tasks)

    def watch_tasks(self, task):
        '''
        Get task status

        :param task: current task
        :type task: Task
        :param over: is task over
        :type over: bool
        '''
        over = False
        finished_date = None
        service = None
        # task['container']['stats'] = self.docker_client.stats(task['container']['id'])
        try:
            service = self.docker_client.services.get(task['container']['meta']['service_id'])
            if service:
                tasks = service.tasks()
                if tasks:
                    service_task = tasks[0]
                    # status = service_task['status']['state']
                    to_update = None
                    self.logger.debug(service_task)
                    if (not task['container']['id']) and 'ContainerStatus' in service_task['Status'] and 'ContainerID' in service_task['Status']['ContainerStatus'] and service_task['Status']['ContainerStatus']['ContainerID']:
                        task['container']['id'] = service_task['Status']['ContainerStatus']['ContainerID']
                        self.logger.debug('%d:containerid:%s' % (task['id'], task['container']['id']))
                        to_update = {'container.id': task['container']['id']}

                    secondary_status = service_task['Status']['State']
                    self.logger.debug('%d:status:%s' % (task['id'], secondary_status))
                    if secondary_status == 'complete':
                        over = True
                        if 'State' not in task['container']['meta']:
                            task['container']['meta']['State'] = {}
                        task['container']['meta']['State']['ExitCode'] = 0
                        finished_date = datetime.datetime.now()
                    if secondary_status == 'failed':
                        over = True
                        if 'State' not in task['container']['meta']:
                            task['container']['meta']['State'] = {}
                        if 'ExitCode' in service_task['Status']['ContainerStatus']:
                            task['container']['meta']['State']['ExitCode'] = service_task['Status']['ContainerStatus']['ExitCode']
                        else:
                            task['container']['meta']['State']['ExitCode'] = 1
                        finished_date = datetime.datetime.now()
                    if secondary_status != task['status']['secondary']:
                        if to_update is None:
                            to_update = {}
                        to_update['status.secondary'] = secondary_status

                    if task['container']['meta']['Node']['Name'] == 'unknown':
                        if 'NodeID' in service_task and service_task['NodeID']:
                            node = self.docker_client.nodes.get(service_task['NodeID'])
                            task['container']['meta']['Node']['Name'] = node.attrs['Status']['Addr']
                            task['container']['ip_address'] = task['container']['meta']['Node']['Name']
                            if to_update is None:
                                to_update = {}
                            to_update['container.meta.Node.Name'] = task['container']['meta']['Node']['Name']
                            to_update['container.ip_address'] = task['container']['ip_address']

                    if 'NetworksAttachments' in service_task and 'Addresses' in service_task['NetworksAttachments'] and service_task['NetworksAttachments']['Addresses']:
                        adresses = service_task['NetworksAttachments']['Addresses'][0].split('/')
                        task['container']['ip_address'] = adresses[0]
                        if to_update is None:
                            to_update = {}
                        to_update['container.ip_address'] = task['container']['ip_address']
                        self.logger.debug('%d:ipaddress:%s' % (task['id'], task['container']['ip_address']))

                    if to_update is not None:
                        for i in range(5):
                            try:
                                self.jobs_handler.update({'id': task['id']}, {'$set': to_update})
                                break
                            except pymongo.errors.AutoReconnect:
                                self.logger.warn('Mongo:AutoReconnect')
                                time.sleep(pow(2, i))

                else:
                    self.logger.debug('%d:no tasks' % (task['id']))
                    over = True
                    finished_date = datetime.datetime.now()

        except Exception:
            self.logger.debug("Could not get container, may be already killed: " + str(task['container']['id']))
            self.logger.exception("failed to get service")
            finished_date = datetime.datetime.now()

        if finished_date and finished_date.year > 1:
            over = True

        if over:
            self.logger.warn('Container:' + str(task['container']['id']) + ':Over')
            try:
                if service:
                    service.remove()
                # self.docker_client.remove_container(task['container']['id'])
            except Exception:
                self.logger.debug('Could not remove container ' + str(task['container']['id']))
            over = True
        else:
            self.logger.debug('Container:' + task['container']['id'] + ':Running')
        return (task, over)

    def kill_task(self, task):
        '''
        Kills a running task

        :param tasks: task to kill
        :type tasks: Task
        :return: (Task, over) over is True if task could be killed
        '''
        over = True
        try:
            service = self.docker_client.services.get(task['container']['meta']['service_id'])
            if service:
                service.remove()
        except Exception as e:
            self.logger.debug("Could not kill container: " + str(task['container']['id']) + ":" + str(e))
            over = False
        return (task, over)

    def suspend_task(self, task):
        '''
        Suspend/pause a task

        :param tasks: task to suspend
        :type tasks: Task
        :return: (Task, over) over is True if task could be suspended
        '''
        self.logger.error('Not supported')
        return (task, False)

    def resume_task(self, task):
        '''
        Resume/restart a task

        :param tasks: task to resumed
        :type tasks: Task
        :return: (Task, over) over is True if task could be resumed
        '''
        self.logger.error('Not supported')
        return (task, False)
